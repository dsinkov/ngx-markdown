import { Pipe, Injectable, Optional, Component, ElementRef, EventEmitter, Input, Output, NgZone, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { parse, Renderer } from 'marked';
import { map } from 'rxjs/operators';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LanguagePipe {
    /**
     * @param {?} value
     * @param {?} language
     * @return {?}
     */
    transform(value, language) {
        if (typeof value !== 'string') {
            console.error(`LanguagePipe has been invoked with an invalid value type [${value}]`);
            return value;
        }
        if (typeof language !== 'string') {
            console.error(`LanguagePipe has been invoked with an invalid parameter [${language}]`);
            return value;
        }
        return '```' + language + '\n' + value + '\n```';
    }
}
LanguagePipe.decorators = [
    { type: Pipe, args: [{
                name: 'language',
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MarkedOptions {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// tslint:disable-next-line:max-line-length
const /** @type {?} */ errorSrcWithoutHttpClient = '[ngx-markdown] When using the [src] attribute you *have to* pass the `HttpClient` as a parameter of the `forRoot` method. See README for more information';
class MarkdownService {
    /**
     * @param {?} http
     * @param {?} options
     */
    constructor(http, options) {
        this.http = http;
        this.options = options;
        if (!this.renderer) {
            this.renderer = new Renderer();
        }
    }
    /**
     * @return {?}
     */
    get renderer() {
        return this.options.renderer;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set renderer(value) {
        this.options.renderer = value;
    }
    /**
     * @param {?} markdown
     * @param {?=} markedOptions
     * @return {?}
     */
    compile(markdown, markedOptions = this.options) {
        const /** @type {?} */ precompiled = this.precompile(markdown);
        return parse(precompiled, markedOptions);
    }
    /**
     * @param {?} src
     * @return {?}
     */
    getSource(src) {
        if (!this.http) {
            throw new Error(errorSrcWithoutHttpClient);
        }
        return this.http
            .get(src, { responseType: 'text' })
            .pipe(map(markdown => this.handleExtension(src, markdown)));
    }
    /**
     * @return {?}
     */
    highlight() {
        if (typeof Prism !== 'undefined') {
            Prism.highlightAll(false);
        }
    }
    /**
     * @param {?} src
     * @param {?} markdown
     * @return {?}
     */
    handleExtension(src, markdown) {
        const /** @type {?} */ extension = src
            ? src.split('.').splice(-1).join()
            : null;
        return extension !== 'md'
            ? '```' + extension + '\n' + markdown + '\n```'
            : markdown;
    }
    /**
     * @param {?} markdown
     * @return {?}
     */
    precompile(markdown) {
        if (!markdown) {
            return '';
        }
        let /** @type {?} */ indentStart;
        return markdown
            .replace(/\&gt;/g, '>')
            .split('\n')
            .map(line => {
            // find position of 1st non-whitespace character
            // to determine the markdown indentation start
            if (line.length > 0 && isNaN(indentStart)) {
                indentStart = line.search(/\S|$/);
            }
            // remove whitespaces before indentation start
            return indentStart
                ? line.substring(indentStart)
                : line;
        }).join('\n');
    }
}
MarkdownService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MarkdownService.ctorParameters = () => [
    { type: HttpClient, decorators: [{ type: Optional }] },
    { type: MarkedOptions }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MarkdownComponent {
    /**
     * @param {?} element
     * @param {?} markdownService
     */
    constructor(element, markdownService) {
        this.element = element;
        this.markdownService = markdownService;
        this.isTargetBlankLinks = false;
        this.error = new EventEmitter();
        this.load = new EventEmitter();
    }
    /**
     * @return {?}
     */
    get data() {
        return this._data;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set data(value) {
        this._data = value;
        this.render(value);
    }
    /**
     * @return {?}
     */
    get src() {
        return this._src;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set src(value) {
        this._src = value;
        this.markdownService
            .getSource(value)
            .subscribe(markdown => {
            this.render(markdown);
            this.load.emit(markdown);
        }, error => this.error.emit(error));
    }
    /**
     * @return {?}
     */
    get isTranscluded() {
        return !this.data && !this.src;
    }
    /**
     * @param {?} href
     * @param {?} title
     * @param {?} text
     * @return {?}
     */
    static addTargetBlank(href, title, text) {
        let /** @type {?} */ out;
        out = '<a href="' + href + '"';
        out += ' target="_blank"';
        if (title) {
            out += ' title="' + title + '"';
        }
        return out + '>' + text + '</a>';
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (this.isTargetBlankLinks) {
            const /** @type {?} */ customRenderer = new Renderer();
            customRenderer.link = MarkdownComponent.addTargetBlank;
            this.markdownService.renderer = customRenderer;
        }
        if (this.isTranscluded) {
            this.render(this.element.nativeElement.innerHTML);
        }
    }
    /**
     * @param {?} markdown
     * @return {?}
     */
    render(markdown) {
        this.element.nativeElement.innerHTML = this.markdownService.compile(markdown);
        this.markdownService.highlight();
    }
}
MarkdownComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line:component-selector
                selector: 'markdown, [markdown]',
                template: '<ng-content></ng-content>',
                styles: [`:host /deep/ table{border-spacing:0;border-collapse:collapse;margin-bottom:16px}:host /deep/ table td,:host /deep/ table th{padding:6px 13px;border:1px solid #ddd}:host /deep/ table td[align=left],:host /deep/ table th[align=left]{text-align:left}:host /deep/ table td[align=center],:host /deep/ table th[align=center]{text-align:center}:host /deep/ table td[align=right],:host /deep/ table th[align=right]{text-align:right}:host /deep/ table tr:nth-child(2n){background-color:rgba(0,0,0,.03)}:host /deep/ blockquote{padding:0 1em;color:rgba(0,0,0,.535);border-left:.25em solid rgba(0,0,0,.11)}`],
                preserveWhitespaces: true,
            },] },
];
/** @nocollapse */
MarkdownComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: MarkdownService }
];
MarkdownComponent.propDecorators = {
    data: [{ type: Input }],
    src: [{ type: Input }],
    isTargetBlankLinks: [{ type: Input }],
    error: [{ type: Output }],
    load: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MarkdownPipe {
    /**
     * @param {?} markdownService
     * @param {?} zone
     */
    constructor(markdownService, zone) {
        this.markdownService = markdownService;
        this.zone = zone;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        if (typeof value !== 'string') {
            console.error(`MarkdownPipe has been invoked with an invalid value type [${value}]`);
            return value;
        }
        const /** @type {?} */ markdown = this.markdownService.compile(value);
        this.zone.runOutsideAngular(() => {
            // glitch in the UI... need a better way to handle this!
            setTimeout(() => this.markdownService.highlight());
        });
        return markdown;
    }
}
MarkdownPipe.decorators = [
    { type: Pipe, args: [{
                name: 'markdown',
            },] },
];
/** @nocollapse */
MarkdownPipe.ctorParameters = () => [
    { type: MarkdownService },
    { type: NgZone }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const /** @type {?} */ initialMarkedOptions = {
    provide: MarkedOptions,
    useValue: {
        gfm: true,
        tables: true,
        breaks: false,
        pedantic: false,
        sanitize: false,
        smartLists: true,
        smartypants: false,
    },
};
const /** @type {?} */ sharedDeclarations = [
    LanguagePipe,
    MarkdownComponent,
    MarkdownPipe,
];
class MarkdownModule {
    /**
     * @param {?=} markdownModuleConfig
     * @return {?}
     */
    static forRoot(markdownModuleConfig) {
        return {
            ngModule: MarkdownModule,
            providers: [
                MarkdownService,
                ...(markdownModuleConfig
                    ? [
                        markdownModuleConfig.loader || [],
                        markdownModuleConfig.markedOptions || initialMarkedOptions,
                    ]
                    : [initialMarkedOptions]),
            ],
        };
    }
    /**
     * @return {?}
     */
    static forChild() {
        return {
            ngModule: MarkdownModule,
        };
    }
}
MarkdownModule.decorators = [
    { type: NgModule, args: [{
                exports: [
                    ...sharedDeclarations,
                ],
                declarations: [
                    ...sharedDeclarations,
                ],
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MarkedRenderer extends Renderer {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { LanguagePipe, MarkdownComponent, initialMarkedOptions, MarkdownModule, MarkdownPipe, errorSrcWithoutHttpClient, MarkdownService, MarkedOptions, MarkedRenderer };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LW1hcmtkb3duLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9uZ3gtbWFya2Rvd24vc3JjL2xhbmd1YWdlLnBpcGUudHMiLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2VkLW9wdGlvbnMudHMiLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2Rvd24uc2VydmljZS50cyIsIm5nOi8vbmd4LW1hcmtkb3duL3NyYy9tYXJrZG93bi5jb21wb25lbnQudHMiLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2Rvd24ucGlwZS50cyIsIm5nOi8vbmd4LW1hcmtkb3duL3NyYy9tYXJrZG93bi5tb2R1bGUudHMiLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2VkLXJlbmRlcmVyLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnbGFuZ3VhZ2UnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFuZ3VhZ2VQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCBsYW5ndWFnZTogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoYExhbmd1YWdlUGlwZSBoYXMgYmVlbiBpbnZva2VkIHdpdGggYW4gaW52YWxpZCB2YWx1ZSB0eXBlIFske3ZhbHVlfV1gKTtcclxuICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiBsYW5ndWFnZSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgY29uc29sZS5lcnJvcihgTGFuZ3VhZ2VQaXBlIGhhcyBiZWVuIGludm9rZWQgd2l0aCBhbiBpbnZhbGlkIHBhcmFtZXRlciBbJHtsYW5ndWFnZX1dYCk7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiAnYGBgJyArIGxhbmd1YWdlICsgJ1xcbicgKyAgdmFsdWUgKyAnXFxuYGBgJztcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgUmVuZGVyZXIgfSBmcm9tICdtYXJrZWQnO1xyXG5cclxuZXhwb3J0IGNsYXNzIE1hcmtlZE9wdGlvbnMgaW1wbGVtZW50cyBtYXJrZWQuTWFya2VkT3B0aW9ucyB7XHJcbiAgLyoqXHJcbiAgICogVHlwZTogb2JqZWN0IERlZmF1bHQ6IG5ldyBSZW5kZXJlcigpXHJcbiAgICpcclxuICAgKiBBbiBvYmplY3QgY29udGFpbmluZyBmdW5jdGlvbnMgdG8gcmVuZGVyIHRva2VucyB0byBIVE1MLlxyXG4gICAqL1xyXG4gIHJlbmRlcmVyPzogUmVuZGVyZXI7XHJcblxyXG4gIC8qKlxyXG4gICAqIEVuYWJsZSBHaXRIdWIgZmxhdm9yZWQgbWFya2Rvd24uXHJcbiAgICovXHJcbiAgZ2ZtPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogRW5hYmxlIEdGTSB0YWJsZXMuIFRoaXMgb3B0aW9uIHJlcXVpcmVzIHRoZSBnZm0gb3B0aW9uIHRvIGJlIHRydWUuXHJcbiAgICovXHJcbiAgdGFibGVzPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogRW5hYmxlIEdGTSBsaW5lIGJyZWFrcy4gVGhpcyBvcHRpb24gcmVxdWlyZXMgdGhlIGdmbSBvcHRpb24gdG8gYmUgdHJ1ZS5cclxuICAgKi9cclxuICBicmVha3M/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBDb25mb3JtIHRvIG9ic2N1cmUgcGFydHMgb2YgbWFya2Rvd24ucGwgYXMgbXVjaCBhcyBwb3NzaWJsZS4gRG9uJ3QgZml4IGFueSBvZiB0aGUgb3JpZ2luYWwgbWFya2Rvd24gYnVncyBvciBwb29yIGJlaGF2aW9yLlxyXG4gICAqL1xyXG4gIHBlZGFudGljPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogU2FuaXRpemUgdGhlIG91dHB1dC4gSWdub3JlIGFueSBIVE1MIHRoYXQgaGFzIGJlZW4gaW5wdXQuXHJcbiAgICovXHJcbiAgc2FuaXRpemU/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBNYW5nbGUgYXV0b2xpbmtzICg8ZW1haWxAZG9tYWluLmNvbT4pLlxyXG4gICAqL1xyXG4gIG1hbmdsZT86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFVzZSBzbWFydGVyIGxpc3QgYmVoYXZpb3IgdGhhbiB0aGUgb3JpZ2luYWwgbWFya2Rvd24uIE1heSBldmVudHVhbGx5IGJlIGRlZmF1bHQgd2l0aCB0aGUgb2xkIGJlaGF2aW9yIG1vdmVkIGludG8gcGVkYW50aWMuXHJcbiAgICovXHJcbiAgc21hcnRMaXN0cz86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFNob3dzIGFuIEhUTUwgZXJyb3IgbWVzc2FnZSB3aGVuIHJlbmRlcmluZyBmYWlscy5cclxuICAgKi9cclxuICBzaWxlbnQ/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIHByZWZpeCBmb3IgY29kZSBibG9jayBjbGFzc2VzLlxyXG4gICAqL1xyXG4gIGxhbmdQcmVmaXg/OiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFVzZSBcInNtYXJ0XCIgdHlwb2dyYWhpYyBwdW5jdHVhdGlvbiBmb3IgdGhpbmdzIGxpa2UgcXVvdGVzIGFuZCBkYXNoZXMuXHJcbiAgICovXHJcbiAgc21hcnR5cGFudHM/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIHByZWZpeCBmb3IgaGVhZGVyIHRhZyBpZHMuXHJcbiAgICovXHJcbiAgaGVhZGVyUHJlZml4Pzogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBHZW5lcmF0ZSBjbG9zaW5nIHNsYXNoIGZvciBzZWxmLWNsb3NpbmcgdGFncyAoPGJyLz4gaW5zdGVhZCBvZiA8YnI+KVxyXG4gICAqL1xyXG4gIHhodG1sPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogQSBmdW5jdGlvbiB0byBoaWdobGlnaHQgY29kZSBibG9ja3MuIFRoZSBmdW5jdGlvbiB0YWtlcyB0aHJlZSBhcmd1bWVudHM6IGNvZGUsIGxhbmcsIGFuZCBjYWxsYmFjay5cclxuICAgKi9cclxuICBoaWdobGlnaHQ/KGNvZGU6IHN0cmluZywgbGFuZzogc3RyaW5nLCBjYWxsYmFjaz86IChlcnJvcjogYW55IHwgdW5kZWZpbmVkLCBjb2RlOiBzdHJpbmcpID0+IHZvaWQpOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIE9wdGlvbmFsbHkgc2FuaXRpemUgZm91bmQgSFRNTCB3aXRoIGEgc2FuaXRpemVyIGZ1bmN0aW9uLlxyXG4gICAqL1xyXG4gIHNhbml0aXplcj8oaHRtbDogc3RyaW5nKTogc3RyaW5nO1xyXG59XHJcbiIsImltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgcGFyc2UsIFJlbmRlcmVyIH0gZnJvbSAnbWFya2VkJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBNYXJrZWRPcHRpb25zIH0gZnJvbSAnLi9tYXJrZWQtb3B0aW9ucyc7XHJcbmltcG9ydCB7IE1hcmtlZFJlbmRlcmVyIH0gZnJvbSAnLi9tYXJrZWQtcmVuZGVyZXInO1xyXG5cclxuZGVjbGFyZSB2YXIgUHJpc206IHtcclxuICBoaWdobGlnaHRBbGw6IChhc3luYzogYm9vbGVhbikgPT4gdm9pZDtcclxufTtcclxuXHJcbi8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcclxuZXhwb3J0IGNvbnN0IGVycm9yU3JjV2l0aG91dEh0dHBDbGllbnQgPSAnW25neC1tYXJrZG93bl0gV2hlbiB1c2luZyB0aGUgW3NyY10gYXR0cmlidXRlIHlvdSAqaGF2ZSB0byogcGFzcyB0aGUgYEh0dHBDbGllbnRgIGFzIGEgcGFyYW1ldGVyIG9mIHRoZSBgZm9yUm9vdGAgbWV0aG9kLiBTZWUgUkVBRE1FIGZvciBtb3JlIGluZm9ybWF0aW9uJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1hcmtkb3duU2VydmljZSB7XHJcbiAgZ2V0IHJlbmRlcmVyKCk6IFJlbmRlcmVyIHtcclxuICAgIHJldHVybiB0aGlzLm9wdGlvbnMucmVuZGVyZXI7XHJcbiAgfVxyXG4gIHNldCByZW5kZXJlcih2YWx1ZTogbWFya2VkLlJlbmRlcmVyKSB7XHJcbiAgICB0aGlzLm9wdGlvbnMucmVuZGVyZXIgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgQE9wdGlvbmFsKCkgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgcHVibGljIG9wdGlvbnM6IE1hcmtlZE9wdGlvbnMsXHJcbiAgKSB7XHJcbiAgICBpZiAoIXRoaXMucmVuZGVyZXIpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlciA9IG5ldyBSZW5kZXJlcigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29tcGlsZShtYXJrZG93bjogc3RyaW5nLCBtYXJrZWRPcHRpb25zID0gdGhpcy5vcHRpb25zKSB7XHJcbiAgICBjb25zdCBwcmVjb21waWxlZCA9IHRoaXMucHJlY29tcGlsZShtYXJrZG93bik7XHJcbiAgICByZXR1cm4gcGFyc2UocHJlY29tcGlsZWQsIG1hcmtlZE9wdGlvbnMpO1xyXG4gIH1cclxuXHJcbiAgZ2V0U291cmNlKHNyYzogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMuaHR0cCkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3JTcmNXaXRob3V0SHR0cENsaWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cFxyXG4gICAgICAuZ2V0KHNyYywgeyByZXNwb25zZVR5cGU6ICd0ZXh0JyB9KVxyXG4gICAgICAucGlwZShtYXAobWFya2Rvd24gPT4gdGhpcy5oYW5kbGVFeHRlbnNpb24oc3JjLCBtYXJrZG93bikpKTtcclxuICB9XHJcblxyXG4gIGhpZ2hsaWdodCgpIHtcclxuICAgIGlmICh0eXBlb2YgUHJpc20gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgIFByaXNtLmhpZ2hsaWdodEFsbChmYWxzZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUV4dGVuc2lvbihzcmM6IHN0cmluZywgbWFya2Rvd246IHN0cmluZykge1xyXG4gICAgY29uc3QgZXh0ZW5zaW9uID0gc3JjXHJcbiAgICAgID8gc3JjLnNwbGl0KCcuJykuc3BsaWNlKC0xKS5qb2luKClcclxuICAgICAgOiBudWxsO1xyXG4gICAgcmV0dXJuIGV4dGVuc2lvbiAhPT0gJ21kJ1xyXG4gICAgICA/ICdgYGAnICsgZXh0ZW5zaW9uICsgJ1xcbicgKyBtYXJrZG93biArICdcXG5gYGAnXHJcbiAgICAgIDogbWFya2Rvd247XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZWNvbXBpbGUobWFya2Rvd246IHN0cmluZykge1xyXG4gICAgaWYgKCFtYXJrZG93bikge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbiAgICBsZXQgaW5kZW50U3RhcnQ6IG51bWJlcjtcclxuICAgIHJldHVybiBtYXJrZG93blxyXG4gICAgICAucmVwbGFjZSgvXFwmZ3Q7L2csICc+JylcclxuICAgICAgLnNwbGl0KCdcXG4nKVxyXG4gICAgICAubWFwKGxpbmUgPT4ge1xyXG4gICAgICAgIC8vIGZpbmQgcG9zaXRpb24gb2YgMXN0IG5vbi13aGl0ZXNwYWNlIGNoYXJhY3RlclxyXG4gICAgICAgIC8vIHRvIGRldGVybWluZSB0aGUgbWFya2Rvd24gaW5kZW50YXRpb24gc3RhcnRcclxuICAgICAgICBpZiAobGluZS5sZW5ndGggPiAwICYmIGlzTmFOKGluZGVudFN0YXJ0KSkge1xyXG4gICAgICAgICAgaW5kZW50U3RhcnQgPSBsaW5lLnNlYXJjaCgvXFxTfCQvKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gcmVtb3ZlIHdoaXRlc3BhY2VzIGJlZm9yZSBpbmRlbnRhdGlvbiBzdGFydFxyXG4gICAgICAgIHJldHVybiBpbmRlbnRTdGFydFxyXG4gICAgICAgICAgPyBsaW5lLnN1YnN0cmluZyhpbmRlbnRTdGFydClcclxuICAgICAgICAgIDogbGluZTtcclxuICAgICAgfSkuam9pbignXFxuJyk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEFmdGVyVmlld0luaXQsIENvbXBvbmVudCwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlbmRlcmVyIH0gZnJvbSAnbWFya2VkJztcclxuaW1wb3J0IHsgTWFya2Rvd25TZXJ2aWNlIH0gZnJvbSAnLi9tYXJrZG93bi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpjb21wb25lbnQtc2VsZWN0b3JcclxuICBzZWxlY3RvcjogJ21hcmtkb3duLCBbbWFya2Rvd25dJyxcclxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxyXG4gIHN0eWxlczogW2A6aG9zdCAvZGVlcC8gdGFibGV7Ym9yZGVyLXNwYWNpbmc6MDtib3JkZXItY29sbGFwc2U6Y29sbGFwc2U7bWFyZ2luLWJvdHRvbToxNnB4fTpob3N0IC9kZWVwLyB0YWJsZSB0ZCw6aG9zdCAvZGVlcC8gdGFibGUgdGh7cGFkZGluZzo2cHggMTNweDtib3JkZXI6MXB4IHNvbGlkICNkZGR9Omhvc3QgL2RlZXAvIHRhYmxlIHRkW2FsaWduPWxlZnRdLDpob3N0IC9kZWVwLyB0YWJsZSB0aFthbGlnbj1sZWZ0XXt0ZXh0LWFsaWduOmxlZnR9Omhvc3QgL2RlZXAvIHRhYmxlIHRkW2FsaWduPWNlbnRlcl0sOmhvc3QgL2RlZXAvIHRhYmxlIHRoW2FsaWduPWNlbnRlcl17dGV4dC1hbGlnbjpjZW50ZXJ9Omhvc3QgL2RlZXAvIHRhYmxlIHRkW2FsaWduPXJpZ2h0XSw6aG9zdCAvZGVlcC8gdGFibGUgdGhbYWxpZ249cmlnaHRde3RleHQtYWxpZ246cmlnaHR9Omhvc3QgL2RlZXAvIHRhYmxlIHRyOm50aC1jaGlsZCgybil7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDAsMCwwLC4wMyl9Omhvc3QgL2RlZXAvIGJsb2NrcXVvdGV7cGFkZGluZzowIDFlbTtjb2xvcjpyZ2JhKDAsMCwwLC41MzUpO2JvcmRlci1sZWZ0Oi4yNWVtIHNvbGlkIHJnYmEoMCwwLDAsLjExKX1gXSxcclxuICBwcmVzZXJ2ZVdoaXRlc3BhY2VzOiB0cnVlLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFya2Rvd25Db21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcclxuICBwcml2YXRlIF9kYXRhOiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBfc3JjOiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IGRhdGEoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9kYXRhO1xyXG4gIH1cclxuXHJcbiAgc2V0IGRhdGEodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5fZGF0YSA9IHZhbHVlO1xyXG4gICAgdGhpcy5yZW5kZXIodmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBnZXQgc3JjKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fc3JjO1xyXG4gIH1cclxuXHJcbiAgc2V0IHNyYyh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLl9zcmMgPSB2YWx1ZTtcclxuICAgIHRoaXMubWFya2Rvd25TZXJ2aWNlXHJcbiAgICAgIC5nZXRTb3VyY2UodmFsdWUpXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgbWFya2Rvd24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5yZW5kZXIobWFya2Rvd24pO1xyXG4gICAgICAgICAgdGhpcy5sb2FkLmVtaXQobWFya2Rvd24pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZXJyb3IgPT4gdGhpcy5lcnJvci5lbWl0KGVycm9yKSxcclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIEBJbnB1dCgpIGlzVGFyZ2V0QmxhbmtMaW5rcyA9IGZhbHNlO1xyXG5cclxuICBAT3V0cHV0KCkgZXJyb3IgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuICBAT3V0cHV0KCkgbG9hZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG5cclxuICBnZXQgaXNUcmFuc2NsdWRlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhdGhpcy5kYXRhICYmICF0aGlzLnNyYztcclxuICB9XHJcblxyXG4gIHN0YXRpYyBhZGRUYXJnZXRCbGFuayhocmVmOiBzdHJpbmcsIHRpdGxlOiBzdHJpbmcsIHRleHQ6IHN0cmluZykge1xyXG4gICAgbGV0IG91dDtcclxuICAgIG91dCA9ICc8YSBocmVmPVwiJyArIGhyZWYgKyAnXCInO1xyXG4gICAgb3V0ICs9ICcgdGFyZ2V0PVwiX2JsYW5rXCInO1xyXG4gICAgaWYgKHRpdGxlKSB7XHJcbiAgICAgIG91dCArPSAnIHRpdGxlPVwiJyArIHRpdGxlICsgJ1wiJztcclxuICAgIH1cclxuICAgIHJldHVybiBvdXQgKyAnPicgKyB0ZXh0ICsgJzwvYT4nO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZWxlbWVudDogRWxlbWVudFJlZixcclxuICAgIHB1YmxpYyBtYXJrZG93blNlcnZpY2U6IE1hcmtkb3duU2VydmljZSxcclxuICApIHtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIGlmICh0aGlzLmlzVGFyZ2V0QmxhbmtMaW5rcykge1xyXG4gICAgICBjb25zdCBjdXN0b21SZW5kZXJlciA9IG5ldyBSZW5kZXJlcigpO1xyXG4gICAgICBjdXN0b21SZW5kZXJlci5saW5rID0gTWFya2Rvd25Db21wb25lbnQuYWRkVGFyZ2V0Qmxhbms7XHJcbiAgICAgIHRoaXMubWFya2Rvd25TZXJ2aWNlLnJlbmRlcmVyID0gY3VzdG9tUmVuZGVyZXI7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5pc1RyYW5zY2x1ZGVkKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyKHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmlubmVySFRNTCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW5kZXIobWFya2Rvd246IHN0cmluZykge1xyXG4gICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MID0gdGhpcy5tYXJrZG93blNlcnZpY2UuY29tcGlsZShtYXJrZG93bik7XHJcbiAgICB0aGlzLm1hcmtkb3duU2VydmljZS5oaWdobGlnaHQoKTtcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgTmdab25lLCBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBNYXJrZG93blNlcnZpY2UgfSBmcm9tICcuL21hcmtkb3duLnNlcnZpY2UnO1xyXG5cclxuQFBpcGUoe1xyXG4gIG5hbWU6ICdtYXJrZG93bicsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXJrZG93blBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIG1hcmtkb3duU2VydmljZTogTWFya2Rvd25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSB6b25lOiBOZ1pvbmUsXHJcbiAgKSB7IH1cclxuXHJcbiAgdHJhbnNmb3JtKHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgY29uc29sZS5lcnJvcihgTWFya2Rvd25QaXBlIGhhcyBiZWVuIGludm9rZWQgd2l0aCBhbiBpbnZhbGlkIHZhbHVlIHR5cGUgWyR7dmFsdWV9XWApO1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgbWFya2Rvd24gPSB0aGlzLm1hcmtkb3duU2VydmljZS5jb21waWxlKHZhbHVlKTtcclxuXHJcbiAgICB0aGlzLnpvbmUucnVuT3V0c2lkZUFuZ3VsYXIoKCkgPT4ge1xyXG4gICAgICAvLyBnbGl0Y2ggaW4gdGhlIFVJLi4uIG5lZWQgYSBiZXR0ZXIgd2F5IHRvIGhhbmRsZSB0aGlzIVxyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMubWFya2Rvd25TZXJ2aWNlLmhpZ2hsaWdodCgpKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBtYXJrZG93bjtcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIFByb3ZpZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBMYW5ndWFnZVBpcGUgfSBmcm9tICcuL2xhbmd1YWdlLnBpcGUnO1xyXG5pbXBvcnQgeyBNYXJrZG93bkNvbXBvbmVudCB9IGZyb20gJy4vbWFya2Rvd24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWFya2Rvd25QaXBlIH0gZnJvbSAnLi9tYXJrZG93bi5waXBlJztcclxuaW1wb3J0IHsgTWFya2Rvd25TZXJ2aWNlIH0gZnJvbSAnLi9tYXJrZG93bi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWFya2VkT3B0aW9ucyB9IGZyb20gJy4vbWFya2VkLW9wdGlvbnMnO1xyXG5cclxuLy8gaGF2aW5nIGEgZGVwZW5kZW5jeSBvbiBgSHR0cENsaWVudE1vZHVsZWAgd2l0aGluIGEgbGlicmFyeVxyXG4vLyBicmVha3MgYWxsIHRoZSBpbnRlcmNlcHRvcnMgZnJvbSB0aGUgYXBwIGNvbnN1bWluZyB0aGUgbGlicmFyeVxyXG4vLyBoZXJlLCB3ZSBleHBsaWNpdGVseSBhc2sgdGhlIHVzZXIgdG8gcGFzcyBhIHByb3ZpZGVyIHdpdGhcclxuLy8gdGhlaXIgb3duIGluc3RhbmNlIG9mIGBIdHRwQ2xpZW50TW9kdWxlYFxyXG5leHBvcnQgaW50ZXJmYWNlIE1hcmtkb3duTW9kdWxlQ29uZmlnIHtcclxuICBsb2FkZXI/OiBQcm92aWRlcjtcclxuICBtYXJrZWRPcHRpb25zPzogUHJvdmlkZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBpbml0aWFsTWFya2VkT3B0aW9uczogUHJvdmlkZXIgPSB7XHJcbiAgcHJvdmlkZTogTWFya2VkT3B0aW9ucyxcclxuICB1c2VWYWx1ZToge1xyXG4gICAgZ2ZtOiB0cnVlLFxyXG4gICAgdGFibGVzOiB0cnVlLFxyXG4gICAgYnJlYWtzOiBmYWxzZSxcclxuICAgIHBlZGFudGljOiBmYWxzZSxcclxuICAgIHNhbml0aXplOiBmYWxzZSxcclxuICAgIHNtYXJ0TGlzdHM6IHRydWUsXHJcbiAgICBzbWFydHlwYW50czogZmFsc2UsXHJcbiAgfSxcclxufTtcclxuXHJcbmNvbnN0IHNoYXJlZERlY2xhcmF0aW9ucyA9IFtcclxuICBMYW5ndWFnZVBpcGUsXHJcbiAgTWFya2Rvd25Db21wb25lbnQsXHJcbiAgTWFya2Rvd25QaXBlLFxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBleHBvcnRzOiBbXHJcbiAgICAuLi5zaGFyZWREZWNsYXJhdGlvbnMsXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIC4uLnNoYXJlZERlY2xhcmF0aW9ucyxcclxuICBdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFya2Rvd25Nb2R1bGUge1xyXG4gIHN0YXRpYyBmb3JSb290KG1hcmtkb3duTW9kdWxlQ29uZmlnPzogTWFya2Rvd25Nb2R1bGVDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBNYXJrZG93bk1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgTWFya2Rvd25TZXJ2aWNlLFxyXG4gICAgICAgIC4uLihtYXJrZG93bk1vZHVsZUNvbmZpZ1xyXG4gICAgICAgICAgPyBbXHJcbiAgICAgICAgICAgICAgbWFya2Rvd25Nb2R1bGVDb25maWcubG9hZGVyIHx8IFtdLFxyXG4gICAgICAgICAgICAgIG1hcmtkb3duTW9kdWxlQ29uZmlnLm1hcmtlZE9wdGlvbnMgfHwgaW5pdGlhbE1hcmtlZE9wdGlvbnMsXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICAgIDogW2luaXRpYWxNYXJrZWRPcHRpb25zXSksXHJcbiAgICAgIF0sXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIGZvckNoaWxkKCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IE1hcmtkb3duTW9kdWxlLFxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgUmVuZGVyZXIgfSBmcm9tICdtYXJrZWQnO1xyXG5cclxuZXhwb3J0IGNsYXNzIE1hcmtlZFJlbmRlcmVyIGV4dGVuZHMgUmVuZGVyZXIgeyB9XHJcbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7Ozs7O0lBT0UsU0FBUyxDQUFDLEtBQWEsRUFBRSxRQUFnQjtRQUN2QyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixPQUFPLENBQUMsS0FBSyxDQUFDLDZEQUE2RCxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ3JGLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxJQUFJLE9BQU8sUUFBUSxLQUFLLFFBQVEsRUFBRTtZQUNoQyxPQUFPLENBQUMsS0FBSyxDQUFDLDREQUE0RCxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZGLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxPQUFPLEtBQUssR0FBRyxRQUFRLEdBQUcsSUFBSSxHQUFJLEtBQUssR0FBRyxPQUFPLENBQUM7S0FDbkQ7OztZQWZGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsVUFBVTthQUNqQjs7Ozs7OztBQ0ZEO0NBNkVDOzs7Ozs7QUMvRUQ7QUFjQSx1QkFBYSx5QkFBeUIsR0FBRywySkFBMkosQ0FBQztBQUdyTTs7Ozs7SUFRRSxZQUNzQixJQUFnQixFQUM3QjtRQURhLFNBQUksR0FBSixJQUFJLENBQVk7UUFDN0IsWUFBTyxHQUFQLE9BQU87UUFFZCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7U0FDaEM7S0FDRjs7OztJQWRELElBQUksUUFBUTtRQUNWLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7S0FDOUI7Ozs7O0lBQ0QsSUFBSSxRQUFRLENBQUMsS0FBc0I7UUFDakMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0tBQy9COzs7Ozs7SUFXRCxPQUFPLENBQUMsUUFBZ0IsRUFBRSxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU87UUFDcEQsdUJBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDOUMsT0FBTyxLQUFLLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQyxDQUFDO0tBQzFDOzs7OztJQUVELFNBQVMsQ0FBQyxHQUFXO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2QsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLENBQUM7YUFDbEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQy9EOzs7O0lBRUQsU0FBUztRQUNQLElBQUksT0FBTyxLQUFLLEtBQUssV0FBVyxFQUFFO1lBQ2hDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0I7S0FDRjs7Ozs7O0lBRU8sZUFBZSxDQUFDLEdBQVcsRUFBRSxRQUFnQjtRQUNuRCx1QkFBTSxTQUFTLEdBQUcsR0FBRztjQUNqQixHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtjQUNoQyxJQUFJLENBQUM7UUFDVCxPQUFPLFNBQVMsS0FBSyxJQUFJO2NBQ3JCLEtBQUssR0FBRyxTQUFTLEdBQUcsSUFBSSxHQUFHLFFBQVEsR0FBRyxPQUFPO2NBQzdDLFFBQVEsQ0FBQzs7Ozs7O0lBR1AsVUFBVSxDQUFDLFFBQWdCO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDYixPQUFPLEVBQUUsQ0FBQztTQUNYO1FBQ0QscUJBQUksV0FBbUIsQ0FBQztRQUN4QixPQUFPLFFBQVE7YUFDWixPQUFPLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQzthQUN0QixLQUFLLENBQUMsSUFBSSxDQUFDO2FBQ1gsR0FBRyxDQUFDLElBQUk7OztZQUdQLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUN6QyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNuQzs7WUFFRCxPQUFPLFdBQVc7a0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7a0JBQzNCLElBQUksQ0FBQztTQUNWLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7WUFsRW5CLFVBQVU7Ozs7WUFoQkYsVUFBVSx1QkEwQmQsUUFBUTtZQXBCSixhQUFhOzs7Ozs7O0FDTnRCOzs7OztJQThERSxZQUNTLFNBQ0E7UUFEQSxZQUFPLEdBQVAsT0FBTztRQUNQLG9CQUFlLEdBQWYsZUFBZTtrQ0FyQk0sS0FBSztxQkFFakIsSUFBSSxZQUFZLEVBQVU7b0JBQzNCLElBQUksWUFBWSxFQUFVO0tBb0IxQzs7OztJQW5ERCxJQUNJLElBQUk7UUFDTixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDbkI7Ozs7O0lBRUQsSUFBSSxJQUFJLENBQUMsS0FBYTtRQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3BCOzs7O0lBRUQsSUFDSSxHQUFHO1FBQ0wsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ2xCOzs7OztJQUVELElBQUksR0FBRyxDQUFDLEtBQWE7UUFDbkIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLGVBQWU7YUFDakIsU0FBUyxDQUFDLEtBQUssQ0FBQzthQUNoQixTQUFTLENBQ1IsUUFBUTtZQUNOLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDMUIsRUFDRCxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQ2hDLENBQUM7S0FDTDs7OztJQU9ELElBQUksYUFBYTtRQUNmLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztLQUNoQzs7Ozs7OztJQUVELE9BQU8sY0FBYyxDQUFDLElBQVksRUFBRSxLQUFhLEVBQUUsSUFBWTtRQUM3RCxxQkFBSSxHQUFHLENBQUM7UUFDUixHQUFHLEdBQUcsV0FBVyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUM7UUFDL0IsR0FBRyxJQUFJLGtCQUFrQixDQUFDO1FBQzFCLElBQUksS0FBSyxFQUFFO1lBQ1QsR0FBRyxJQUFJLFVBQVUsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQ2pDO1FBQ0QsT0FBTyxHQUFHLEdBQUcsR0FBRyxHQUFHLElBQUksR0FBRyxNQUFNLENBQUM7S0FDbEM7Ozs7SUFRRCxlQUFlO1FBQ2IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDM0IsdUJBQU0sY0FBYyxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDdEMsY0FBYyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7WUFDdkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDbkQ7S0FDRjs7Ozs7SUFFRCxNQUFNLENBQUMsUUFBZ0I7UUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlFLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLENBQUM7S0FDbEM7OztZQTlFRixTQUFTLFNBQUM7O2dCQUVULFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLE1BQU0sRUFBRSxDQUFDLG9sQkFBb2xCLENBQUM7Z0JBQzlsQixtQkFBbUIsRUFBRSxJQUFJO2FBQzFCOzs7O1lBVmtDLFVBQVU7WUFFcEMsZUFBZTs7O21CQWFyQixLQUFLO2tCQVVMLEtBQUs7aUNBa0JMLEtBQUs7b0JBRUwsTUFBTTttQkFDTixNQUFNOzs7Ozs7O0FDOUNUOzs7OztJQVNFLFlBQ1UsaUJBQ0E7UUFEQSxvQkFBZSxHQUFmLGVBQWU7UUFDZixTQUFJLEdBQUosSUFBSTtLQUNUOzs7OztJQUVMLFNBQVMsQ0FBQyxLQUFhO1FBQ3JCLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkRBQTZELEtBQUssR0FBRyxDQUFDLENBQUM7WUFDckYsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUVELHVCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVyRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDOztZQUUxQixVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7U0FDcEQsQ0FBQyxDQUFDO1FBRUgsT0FBTyxRQUFRLENBQUM7S0FDakI7OztZQXhCRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLFVBQVU7YUFDakI7Ozs7WUFKUSxlQUFlO1lBRmYsTUFBTTs7Ozs7OztBQ0NmLHVCQWlCYSxvQkFBb0IsR0FBYTtJQUM1QyxPQUFPLEVBQUUsYUFBYTtJQUN0QixRQUFRLEVBQUU7UUFDUixHQUFHLEVBQUUsSUFBSTtRQUNULE1BQU0sRUFBRSxJQUFJO1FBQ1osTUFBTSxFQUFFLEtBQUs7UUFDYixRQUFRLEVBQUUsS0FBSztRQUNmLFFBQVEsRUFBRSxLQUFLO1FBQ2YsVUFBVSxFQUFFLElBQUk7UUFDaEIsV0FBVyxFQUFFLEtBQUs7S0FDbkI7Q0FDRixDQUFDO0FBRUYsdUJBQU0sa0JBQWtCLEdBQUc7SUFDekIsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixZQUFZO0NBQ2IsQ0FBQztBQVVGOzs7OztJQUNFLE9BQU8sT0FBTyxDQUFDLG9CQUEyQztRQUN4RCxPQUFPO1lBQ0wsUUFBUSxFQUFFLGNBQWM7WUFDeEIsU0FBUyxFQUFFO2dCQUNULGVBQWU7Z0JBQ2YsSUFBSSxvQkFBb0I7c0JBQ3BCO3dCQUNFLG9CQUFvQixDQUFDLE1BQU0sSUFBSSxFQUFFO3dCQUNqQyxvQkFBb0IsQ0FBQyxhQUFhLElBQUksb0JBQW9CO3FCQUMzRDtzQkFDRCxDQUFDLG9CQUFvQixDQUFDLENBQUM7YUFDNUI7U0FDRixDQUFDO0tBQ0g7Ozs7SUFFRCxPQUFPLFFBQVE7UUFDYixPQUFPO1lBQ0wsUUFBUSxFQUFFLGNBQWM7U0FDekIsQ0FBQztLQUNIOzs7WUE1QkYsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRTtvQkFDUCxHQUFHLGtCQUFrQjtpQkFDdEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLEdBQUcsa0JBQWtCO2lCQUN0QjthQUNGOzs7Ozs7O0FDNUNELG9CQUU0QixTQUFRLFFBQVE7Q0FBSTs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==