/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { parse, Renderer } from 'marked';
import { map } from 'rxjs/operators';
import { MarkedOptions } from './marked-options';
// tslint:disable-next-line:max-line-length
export var /** @type {?} */ errorSrcWithoutHttpClient = '[ngx-markdown] When using the [src] attribute you *have to* pass the `HttpClient` as a parameter of the `forRoot` method. See README for more information';
var MarkdownService = /** @class */ (function () {
    function MarkdownService(http, options) {
        this.http = http;
        this.options = options;
        if (!this.renderer) {
            this.renderer = new Renderer();
        }
    }
    Object.defineProperty(MarkdownService.prototype, "renderer", {
        get: /**
         * @return {?}
         */
        function () {
            return this.options.renderer;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.options.renderer = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} markdown
     * @param {?=} markedOptions
     * @return {?}
     */
    MarkdownService.prototype.compile = /**
     * @param {?} markdown
     * @param {?=} markedOptions
     * @return {?}
     */
    function (markdown, markedOptions) {
        if (markedOptions === void 0) { markedOptions = this.options; }
        var /** @type {?} */ precompiled = this.precompile(markdown);
        return parse(precompiled, markedOptions);
    };
    /**
     * @param {?} src
     * @return {?}
     */
    MarkdownService.prototype.getSource = /**
     * @param {?} src
     * @return {?}
     */
    function (src) {
        var _this = this;
        if (!this.http) {
            throw new Error(errorSrcWithoutHttpClient);
        }
        return this.http
            .get(src, { responseType: 'text' })
            .pipe(map(function (markdown) { return _this.handleExtension(src, markdown); }));
    };
    /**
     * @return {?}
     */
    MarkdownService.prototype.highlight = /**
     * @return {?}
     */
    function () {
        if (typeof Prism !== 'undefined') {
            Prism.highlightAll(false);
        }
    };
    /**
     * @param {?} src
     * @param {?} markdown
     * @return {?}
     */
    MarkdownService.prototype.handleExtension = /**
     * @param {?} src
     * @param {?} markdown
     * @return {?}
     */
    function (src, markdown) {
        var /** @type {?} */ extension = src
            ? src.split('.').splice(-1).join()
            : null;
        return extension !== 'md'
            ? '```' + extension + '\n' + markdown + '\n```'
            : markdown;
    };
    /**
     * @param {?} markdown
     * @return {?}
     */
    MarkdownService.prototype.precompile = /**
     * @param {?} markdown
     * @return {?}
     */
    function (markdown) {
        if (!markdown) {
            return '';
        }
        var /** @type {?} */ indentStart;
        return markdown
            .replace(/\&gt;/g, '>')
            .split('\n')
            .map(function (line) {
            // find position of 1st non-whitespace character
            // to determine the markdown indentation start
            if (line.length > 0 && isNaN(indentStart)) {
                indentStart = line.search(/\S|$/);
            }
            // remove whitespaces before indentation start
            return indentStart
                ? line.substring(indentStart)
                : line;
        }).join('\n');
    };
    MarkdownService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    MarkdownService.ctorParameters = function () { return [
        { type: HttpClient, decorators: [{ type: Optional }] },
        { type: MarkedOptions }
    ]; };
    return MarkdownService;
}());
export { MarkdownService };
function MarkdownService_tsickle_Closure_declarations() {
    /** @type {?} */
    MarkdownService.prototype.http;
    /** @type {?} */
    MarkdownService.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2Rvd24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYXJrZG93bi8iLCJzb3VyY2VzIjpbInNyYy9tYXJrZG93bi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFVLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFekMsT0FBTyxFQUFjLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWpELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQzs7QUFRakQsTUFBTSxDQUFDLHFCQUFNLHlCQUF5QixHQUFHLDJKQUEySixDQUFDOztJQVduTSx5QkFDc0IsSUFBZ0IsRUFDN0I7UUFEYSxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQzdCLFlBQU8sR0FBUCxPQUFPO1FBRWQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7U0FDaEM7S0FDRjtJQWRELHNCQUFJLHFDQUFROzs7O1FBQVo7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7U0FDOUI7Ozs7O1FBQ0QsVUFBYSxLQUFzQjtZQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7U0FDL0I7OztPQUhBOzs7Ozs7SUFjRCxpQ0FBTzs7Ozs7SUFBUCxVQUFRLFFBQWdCLEVBQUUsYUFBNEI7UUFBNUIsOEJBQUEsRUFBQSxnQkFBZ0IsSUFBSSxDQUFDLE9BQU87UUFDcEQscUJBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDOUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsYUFBYSxDQUFDLENBQUM7S0FDMUM7Ozs7O0lBRUQsbUNBQVM7Ozs7SUFBVCxVQUFVLEdBQVc7UUFBckIsaUJBUUM7UUFQQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2YsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsQ0FBQzthQUNsQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLEVBQW5DLENBQW1DLENBQUMsQ0FBQyxDQUFDO0tBQy9EOzs7O0lBRUQsbUNBQVM7OztJQUFUO1FBQ0UsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNqQyxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNCO0tBQ0Y7Ozs7OztJQUVPLHlDQUFlOzs7OztjQUFDLEdBQVcsRUFBRSxRQUFnQjtRQUNuRCxxQkFBTSxTQUFTLEdBQUcsR0FBRztZQUNuQixDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7WUFDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNULE1BQU0sQ0FBQyxTQUFTLEtBQUssSUFBSTtZQUN2QixDQUFDLENBQUMsS0FBSyxHQUFHLFNBQVMsR0FBRyxJQUFJLEdBQUcsUUFBUSxHQUFHLE9BQU87WUFDL0MsQ0FBQyxDQUFDLFFBQVEsQ0FBQzs7Ozs7O0lBR1Asb0NBQVU7Ozs7Y0FBQyxRQUFnQjtRQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDZCxNQUFNLENBQUMsRUFBRSxDQUFDO1NBQ1g7UUFDRCxxQkFBSSxXQUFtQixDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxRQUFRO2FBQ1osT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUM7YUFDdEIsS0FBSyxDQUFDLElBQUksQ0FBQzthQUNYLEdBQUcsQ0FBQyxVQUFBLElBQUk7OztZQUdQLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ25DOztZQUVELE1BQU0sQ0FBQyxXQUFXO2dCQUNoQixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDVixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzs7Z0JBbEVuQixVQUFVOzs7O2dCQWhCRixVQUFVLHVCQTBCZCxRQUFRO2dCQXBCSixhQUFhOzswQkFOdEI7O1NBaUJhLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IHBhcnNlLCBSZW5kZXJlciB9IGZyb20gJ21hcmtlZCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgTWFya2VkT3B0aW9ucyB9IGZyb20gJy4vbWFya2VkLW9wdGlvbnMnO1xyXG5pbXBvcnQgeyBNYXJrZWRSZW5kZXJlciB9IGZyb20gJy4vbWFya2VkLXJlbmRlcmVyJztcclxuXHJcbmRlY2xhcmUgdmFyIFByaXNtOiB7XHJcbiAgaGlnaGxpZ2h0QWxsOiAoYXN5bmM6IGJvb2xlYW4pID0+IHZvaWQ7XHJcbn07XHJcblxyXG4vLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXHJcbmV4cG9ydCBjb25zdCBlcnJvclNyY1dpdGhvdXRIdHRwQ2xpZW50ID0gJ1tuZ3gtbWFya2Rvd25dIFdoZW4gdXNpbmcgdGhlIFtzcmNdIGF0dHJpYnV0ZSB5b3UgKmhhdmUgdG8qIHBhc3MgdGhlIGBIdHRwQ2xpZW50YCBhcyBhIHBhcmFtZXRlciBvZiB0aGUgYGZvclJvb3RgIG1ldGhvZC4gU2VlIFJFQURNRSBmb3IgbW9yZSBpbmZvcm1hdGlvbic7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBNYXJrZG93blNlcnZpY2Uge1xyXG4gIGdldCByZW5kZXJlcigpOiBSZW5kZXJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5vcHRpb25zLnJlbmRlcmVyO1xyXG4gIH1cclxuICBzZXQgcmVuZGVyZXIodmFsdWU6IG1hcmtlZC5SZW5kZXJlcikge1xyXG4gICAgdGhpcy5vcHRpb25zLnJlbmRlcmVyID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIEBPcHRpb25hbCgpIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcclxuICAgIHB1YmxpYyBvcHRpb25zOiBNYXJrZWRPcHRpb25zLFxyXG4gICkge1xyXG4gICAgaWYgKCF0aGlzLnJlbmRlcmVyKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIgPSBuZXcgUmVuZGVyZXIoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNvbXBpbGUobWFya2Rvd246IHN0cmluZywgbWFya2VkT3B0aW9ucyA9IHRoaXMub3B0aW9ucykge1xyXG4gICAgY29uc3QgcHJlY29tcGlsZWQgPSB0aGlzLnByZWNvbXBpbGUobWFya2Rvd24pO1xyXG4gICAgcmV0dXJuIHBhcnNlKHByZWNvbXBpbGVkLCBtYXJrZWRPcHRpb25zKTtcclxuICB9XHJcblxyXG4gIGdldFNvdXJjZShzcmM6IHN0cmluZykge1xyXG4gICAgaWYgKCF0aGlzLmh0dHApIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGVycm9yU3JjV2l0aG91dEh0dHBDbGllbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0aGlzLmh0dHBcclxuICAgICAgLmdldChzcmMsIHsgcmVzcG9uc2VUeXBlOiAndGV4dCcgfSlcclxuICAgICAgLnBpcGUobWFwKG1hcmtkb3duID0+IHRoaXMuaGFuZGxlRXh0ZW5zaW9uKHNyYywgbWFya2Rvd24pKSk7XHJcbiAgfVxyXG5cclxuICBoaWdobGlnaHQoKSB7XHJcbiAgICBpZiAodHlwZW9mIFByaXNtICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICBQcmlzbS5oaWdobGlnaHRBbGwoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBoYW5kbGVFeHRlbnNpb24oc3JjOiBzdHJpbmcsIG1hcmtkb3duOiBzdHJpbmcpIHtcclxuICAgIGNvbnN0IGV4dGVuc2lvbiA9IHNyY1xyXG4gICAgICA/IHNyYy5zcGxpdCgnLicpLnNwbGljZSgtMSkuam9pbigpXHJcbiAgICAgIDogbnVsbDtcclxuICAgIHJldHVybiBleHRlbnNpb24gIT09ICdtZCdcclxuICAgICAgPyAnYGBgJyArIGV4dGVuc2lvbiArICdcXG4nICsgbWFya2Rvd24gKyAnXFxuYGBgJ1xyXG4gICAgICA6IG1hcmtkb3duO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwcmVjb21waWxlKG1hcmtkb3duOiBzdHJpbmcpIHtcclxuICAgIGlmICghbWFya2Rvd24pIHtcclxuICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG4gICAgbGV0IGluZGVudFN0YXJ0OiBudW1iZXI7XHJcbiAgICByZXR1cm4gbWFya2Rvd25cclxuICAgICAgLnJlcGxhY2UoL1xcJmd0Oy9nLCAnPicpXHJcbiAgICAgIC5zcGxpdCgnXFxuJylcclxuICAgICAgLm1hcChsaW5lID0+IHtcclxuICAgICAgICAvLyBmaW5kIHBvc2l0aW9uIG9mIDFzdCBub24td2hpdGVzcGFjZSBjaGFyYWN0ZXJcclxuICAgICAgICAvLyB0byBkZXRlcm1pbmUgdGhlIG1hcmtkb3duIGluZGVudGF0aW9uIHN0YXJ0XHJcbiAgICAgICAgaWYgKGxpbmUubGVuZ3RoID4gMCAmJiBpc05hTihpbmRlbnRTdGFydCkpIHtcclxuICAgICAgICAgIGluZGVudFN0YXJ0ID0gbGluZS5zZWFyY2goL1xcU3wkLyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHJlbW92ZSB3aGl0ZXNwYWNlcyBiZWZvcmUgaW5kZW50YXRpb24gc3RhcnRcclxuICAgICAgICByZXR1cm4gaW5kZW50U3RhcnRcclxuICAgICAgICAgID8gbGluZS5zdWJzdHJpbmcoaW5kZW50U3RhcnQpXHJcbiAgICAgICAgICA6IGxpbmU7XHJcbiAgICAgIH0pLmpvaW4oJ1xcbicpO1xyXG4gIH1cclxufVxyXG4iXX0=