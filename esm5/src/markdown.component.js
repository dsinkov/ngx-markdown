/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { Renderer } from 'marked';
import { MarkdownService } from './markdown.service';
var MarkdownComponent = /** @class */ (function () {
    function MarkdownComponent(element, markdownService) {
        this.element = element;
        this.markdownService = markdownService;
        this.isTargetBlankLinks = false;
        this.error = new EventEmitter();
        this.load = new EventEmitter();
    }
    Object.defineProperty(MarkdownComponent.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return this._data;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._data = value;
            this.render(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MarkdownComponent.prototype, "src", {
        get: /**
         * @return {?}
         */
        function () {
            return this._src;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            var _this = this;
            this._src = value;
            this.markdownService
                .getSource(value)
                .subscribe(function (markdown) {
                _this.render(markdown);
                _this.load.emit(markdown);
            }, function (error) { return _this.error.emit(error); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MarkdownComponent.prototype, "isTranscluded", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.data && !this.src;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} href
     * @param {?} title
     * @param {?} text
     * @return {?}
     */
    MarkdownComponent.addTargetBlank = /**
     * @param {?} href
     * @param {?} title
     * @param {?} text
     * @return {?}
     */
    function (href, title, text) {
        var /** @type {?} */ out;
        out = '<a href="' + href + '"';
        out += ' target="_blank"';
        if (title) {
            out += ' title="' + title + '"';
        }
        return out + '>' + text + '</a>';
    };
    /**
     * @return {?}
     */
    MarkdownComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        if (this.isTargetBlankLinks) {
            var /** @type {?} */ customRenderer = new Renderer();
            customRenderer.link = MarkdownComponent.addTargetBlank;
            this.markdownService.renderer = customRenderer;
        }
        if (this.isTranscluded) {
            this.render(this.element.nativeElement.innerHTML);
        }
    };
    /**
     * @param {?} markdown
     * @return {?}
     */
    MarkdownComponent.prototype.render = /**
     * @param {?} markdown
     * @return {?}
     */
    function (markdown) {
        this.element.nativeElement.innerHTML = this.markdownService.compile(markdown);
        this.markdownService.highlight();
    };
    MarkdownComponent.decorators = [
        { type: Component, args: [{
                    // tslint:disable-next-line:component-selector
                    selector: 'markdown, [markdown]',
                    template: '<ng-content></ng-content>',
                    styles: [":host /deep/ table{border-spacing:0;border-collapse:collapse;margin-bottom:16px}:host /deep/ table td,:host /deep/ table th{padding:6px 13px;border:1px solid #ddd}:host /deep/ table td[align=left],:host /deep/ table th[align=left]{text-align:left}:host /deep/ table td[align=center],:host /deep/ table th[align=center]{text-align:center}:host /deep/ table td[align=right],:host /deep/ table th[align=right]{text-align:right}:host /deep/ table tr:nth-child(2n){background-color:rgba(0,0,0,.03)}:host /deep/ blockquote{padding:0 1em;color:rgba(0,0,0,.535);border-left:.25em solid rgba(0,0,0,.11)}"],
                    preserveWhitespaces: true,
                },] },
    ];
    /** @nocollapse */
    MarkdownComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: MarkdownService }
    ]; };
    MarkdownComponent.propDecorators = {
        data: [{ type: Input }],
        src: [{ type: Input }],
        isTargetBlankLinks: [{ type: Input }],
        error: [{ type: Output }],
        load: [{ type: Output }]
    };
    return MarkdownComponent;
}());
export { MarkdownComponent };
function MarkdownComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    MarkdownComponent.prototype._data;
    /** @type {?} */
    MarkdownComponent.prototype._src;
    /** @type {?} */
    MarkdownComponent.prototype.isTargetBlankLinks;
    /** @type {?} */
    MarkdownComponent.prototype.error;
    /** @type {?} */
    MarkdownComponent.prototype.load;
    /** @type {?} */
    MarkdownComponent.prototype.element;
    /** @type {?} */
    MarkdownComponent.prototype.markdownService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2Rvd24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1hcmtkb3duLyIsInNvdXJjZXMiOlsic3JjL21hcmtkb3duLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFpQixTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDbEMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDOztJQTREbkQsMkJBQ1MsU0FDQTtRQURBLFlBQU8sR0FBUCxPQUFPO1FBQ1Asb0JBQWUsR0FBZixlQUFlO2tDQXJCTSxLQUFLO3FCQUVqQixJQUFJLFlBQVksRUFBVTtvQkFDM0IsSUFBSSxZQUFZLEVBQVU7S0FvQjFDO0lBbkRELHNCQUNJLG1DQUFJOzs7O1FBRFI7WUFFRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjs7Ozs7UUFFRCxVQUFTLEtBQWE7WUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQjs7O09BTEE7SUFPRCxzQkFDSSxrQ0FBRzs7OztRQURQO1lBRUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDbEI7Ozs7O1FBRUQsVUFBUSxLQUFhO1lBQXJCLGlCQVdDO1lBVkMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7WUFDbEIsSUFBSSxDQUFDLGVBQWU7aUJBQ2pCLFNBQVMsQ0FBQyxLQUFLLENBQUM7aUJBQ2hCLFNBQVMsQ0FDUixVQUFBLFFBQVE7Z0JBQ04sS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDMUIsRUFDRCxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUF0QixDQUFzQixDQUNoQyxDQUFDO1NBQ0w7OztPQWJBO0lBb0JELHNCQUFJLDRDQUFhOzs7O1FBQWpCO1lBQ0UsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7U0FDaEM7OztPQUFBOzs7Ozs7O0lBRU0sZ0NBQWM7Ozs7OztJQUFyQixVQUFzQixJQUFZLEVBQUUsS0FBYSxFQUFFLElBQVk7UUFDN0QscUJBQUksR0FBRyxDQUFDO1FBQ1IsR0FBRyxHQUFHLFdBQVcsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQy9CLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQztRQUMxQixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1YsR0FBRyxJQUFJLFVBQVUsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQ2pDO1FBQ0QsTUFBTSxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQztLQUNsQzs7OztJQVFELDJDQUFlOzs7SUFBZjtRQUNFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDNUIscUJBQU0sY0FBYyxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDdEMsY0FBYyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7WUFDdkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDO1NBQ2hEO1FBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNuRDtLQUNGOzs7OztJQUVELGtDQUFNOzs7O0lBQU4sVUFBTyxRQUFnQjtRQUNyQixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztLQUNsQzs7Z0JBOUVGLFNBQVMsU0FBQzs7b0JBRVQsUUFBUSxFQUFFLHNCQUFzQjtvQkFDaEMsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsb2xCQUFvbEIsQ0FBQztvQkFDOWxCLG1CQUFtQixFQUFFLElBQUk7aUJBQzFCOzs7O2dCQVZrQyxVQUFVO2dCQUVwQyxlQUFlOzs7dUJBYXJCLEtBQUs7c0JBVUwsS0FBSztxQ0FrQkwsS0FBSzt3QkFFTCxNQUFNO3VCQUNOLE1BQU07OzRCQTlDVDs7U0FXYSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LCBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZW5kZXJlciB9IGZyb20gJ21hcmtlZCc7XHJcbmltcG9ydCB7IE1hcmtkb3duU2VydmljZSB9IGZyb20gJy4vbWFya2Rvd24uc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6Y29tcG9uZW50LXNlbGVjdG9yXHJcbiAgc2VsZWN0b3I6ICdtYXJrZG93biwgW21hcmtkb3duXScsXHJcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcclxuICBzdHlsZXM6IFtgOmhvc3QgL2RlZXAvIHRhYmxle2JvcmRlci1zcGFjaW5nOjA7Ym9yZGVyLWNvbGxhcHNlOmNvbGxhcHNlO21hcmdpbi1ib3R0b206MTZweH06aG9zdCAvZGVlcC8gdGFibGUgdGQsOmhvc3QgL2RlZXAvIHRhYmxlIHRoe3BhZGRpbmc6NnB4IDEzcHg7Ym9yZGVyOjFweCBzb2xpZCAjZGRkfTpob3N0IC9kZWVwLyB0YWJsZSB0ZFthbGlnbj1sZWZ0XSw6aG9zdCAvZGVlcC8gdGFibGUgdGhbYWxpZ249bGVmdF17dGV4dC1hbGlnbjpsZWZ0fTpob3N0IC9kZWVwLyB0YWJsZSB0ZFthbGlnbj1jZW50ZXJdLDpob3N0IC9kZWVwLyB0YWJsZSB0aFthbGlnbj1jZW50ZXJde3RleHQtYWxpZ246Y2VudGVyfTpob3N0IC9kZWVwLyB0YWJsZSB0ZFthbGlnbj1yaWdodF0sOmhvc3QgL2RlZXAvIHRhYmxlIHRoW2FsaWduPXJpZ2h0XXt0ZXh0LWFsaWduOnJpZ2h0fTpob3N0IC9kZWVwLyB0YWJsZSB0cjpudGgtY2hpbGQoMm4pe2JhY2tncm91bmQtY29sb3I6cmdiYSgwLDAsMCwuMDMpfTpob3N0IC9kZWVwLyBibG9ja3F1b3Rle3BhZGRpbmc6MCAxZW07Y29sb3I6cmdiYSgwLDAsMCwuNTM1KTtib3JkZXItbGVmdDouMjVlbSBzb2xpZCByZ2JhKDAsMCwwLC4xMSl9YF0sXHJcbiAgcHJlc2VydmVXaGl0ZXNwYWNlczogdHJ1ZSxcclxufSlcclxuZXhwb3J0IGNsYXNzIE1hcmtkb3duQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgcHJpdmF0ZSBfZGF0YTogc3RyaW5nO1xyXG4gIHByaXZhdGUgX3NyYzogc3RyaW5nO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIGdldCBkYXRhKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fZGF0YTtcclxuICB9XHJcblxyXG4gIHNldCBkYXRhKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuX2RhdGEgPSB2YWx1ZTtcclxuICAgIHRoaXMucmVuZGVyKHZhbHVlKTtcclxuICB9XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHNyYygpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMuX3NyYztcclxuICB9XHJcblxyXG4gIHNldCBzcmModmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5fc3JjID0gdmFsdWU7XHJcbiAgICB0aGlzLm1hcmtkb3duU2VydmljZVxyXG4gICAgICAuZ2V0U291cmNlKHZhbHVlKVxyXG4gICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgIG1hcmtkb3duID0+IHtcclxuICAgICAgICAgIHRoaXMucmVuZGVyKG1hcmtkb3duKTtcclxuICAgICAgICAgIHRoaXMubG9hZC5lbWl0KG1hcmtkb3duKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yID0+IHRoaXMuZXJyb3IuZW1pdChlcnJvciksXHJcbiAgICAgICk7XHJcbiAgfVxyXG5cclxuICBASW5wdXQoKSBpc1RhcmdldEJsYW5rTGlua3MgPSBmYWxzZTtcclxuXHJcbiAgQE91dHB1dCgpIGVycm9yID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XHJcbiAgQE91dHB1dCgpIGxvYWQgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgZ2V0IGlzVHJhbnNjbHVkZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gIXRoaXMuZGF0YSAmJiAhdGhpcy5zcmM7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgYWRkVGFyZ2V0QmxhbmsoaHJlZjogc3RyaW5nLCB0aXRsZTogc3RyaW5nLCB0ZXh0OiBzdHJpbmcpIHtcclxuICAgIGxldCBvdXQ7XHJcbiAgICBvdXQgPSAnPGEgaHJlZj1cIicgKyBocmVmICsgJ1wiJztcclxuICAgIG91dCArPSAnIHRhcmdldD1cIl9ibGFua1wiJztcclxuICAgIGlmICh0aXRsZSkge1xyXG4gICAgICBvdXQgKz0gJyB0aXRsZT1cIicgKyB0aXRsZSArICdcIic7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gb3V0ICsgJz4nICsgdGV4dCArICc8L2E+JztcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGVsZW1lbnQ6IEVsZW1lbnRSZWYsXHJcbiAgICBwdWJsaWMgbWFya2Rvd25TZXJ2aWNlOiBNYXJrZG93blNlcnZpY2UsXHJcbiAgKSB7XHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICBpZiAodGhpcy5pc1RhcmdldEJsYW5rTGlua3MpIHtcclxuICAgICAgY29uc3QgY3VzdG9tUmVuZGVyZXIgPSBuZXcgUmVuZGVyZXIoKTtcclxuICAgICAgY3VzdG9tUmVuZGVyZXIubGluayA9IE1hcmtkb3duQ29tcG9uZW50LmFkZFRhcmdldEJsYW5rO1xyXG4gICAgICB0aGlzLm1hcmtkb3duU2VydmljZS5yZW5kZXJlciA9IGN1c3RvbVJlbmRlcmVyO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuaXNUcmFuc2NsdWRlZCkge1xyXG4gICAgICB0aGlzLnJlbmRlcih0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5pbm5lckhUTUwpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVuZGVyKG1hcmtkb3duOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmlubmVySFRNTCA9IHRoaXMubWFya2Rvd25TZXJ2aWNlLmNvbXBpbGUobWFya2Rvd24pO1xyXG4gICAgdGhpcy5tYXJrZG93blNlcnZpY2UuaGlnaGxpZ2h0KCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==