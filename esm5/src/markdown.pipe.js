/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgZone, Pipe } from '@angular/core';
import { MarkdownService } from './markdown.service';
var MarkdownPipe = /** @class */ (function () {
    function MarkdownPipe(markdownService, zone) {
        this.markdownService = markdownService;
        this.zone = zone;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    MarkdownPipe.prototype.transform = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var _this = this;
        if (typeof value !== 'string') {
            console.error("MarkdownPipe has been invoked with an invalid value type [" + value + "]");
            return value;
        }
        var /** @type {?} */ markdown = this.markdownService.compile(value);
        this.zone.runOutsideAngular(function () {
            // glitch in the UI... need a better way to handle this!
            setTimeout(function () { return _this.markdownService.highlight(); });
        });
        return markdown;
    };
    MarkdownPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'markdown',
                },] },
    ];
    /** @nocollapse */
    MarkdownPipe.ctorParameters = function () { return [
        { type: MarkdownService },
        { type: NgZone }
    ]; };
    return MarkdownPipe;
}());
export { MarkdownPipe };
function MarkdownPipe_tsickle_Closure_declarations() {
    /** @type {?} */
    MarkdownPipe.prototype.markdownService;
    /** @type {?} */
    MarkdownPipe.prototype.zone;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2Rvd24ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYXJrZG93bi8iLCJzb3VyY2VzIjpbInNyYy9tYXJrZG93bi5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDOztJQU9uRCxzQkFDVSxpQkFDQTtRQURBLG9CQUFlLEdBQWYsZUFBZTtRQUNmLFNBQUksR0FBSixJQUFJO0tBQ1Q7Ozs7O0lBRUwsZ0NBQVM7Ozs7SUFBVCxVQUFVLEtBQWE7UUFBdkIsaUJBY0M7UUFiQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsK0RBQTZELEtBQUssTUFBRyxDQUFDLENBQUM7WUFDckYsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUNkO1FBRUQscUJBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXJELElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7O1lBRTFCLFVBQVUsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO1NBQ3BELENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxRQUFRLENBQUM7S0FDakI7O2dCQXhCRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLFVBQVU7aUJBQ2pCOzs7O2dCQUpRLGVBQWU7Z0JBRmYsTUFBTTs7dUJBQWY7O1NBT2EsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nWm9uZSwgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTWFya2Rvd25TZXJ2aWNlIH0gZnJvbSAnLi9tYXJrZG93bi5zZXJ2aWNlJztcclxuXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnbWFya2Rvd24nLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFya2Rvd25QaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBtYXJrZG93blNlcnZpY2U6IE1hcmtkb3duU2VydmljZSxcclxuICAgIHByaXZhdGUgem9uZTogTmdab25lLFxyXG4gICkgeyB9XHJcblxyXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoYE1hcmtkb3duUGlwZSBoYXMgYmVlbiBpbnZva2VkIHdpdGggYW4gaW52YWxpZCB2YWx1ZSB0eXBlIFske3ZhbHVlfV1gKTtcclxuICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IG1hcmtkb3duID0gdGhpcy5tYXJrZG93blNlcnZpY2UuY29tcGlsZSh2YWx1ZSk7XHJcblxyXG4gICAgdGhpcy56b25lLnJ1bk91dHNpZGVBbmd1bGFyKCgpID0+IHtcclxuICAgICAgLy8gZ2xpdGNoIGluIHRoZSBVSS4uLiBuZWVkIGEgYmV0dGVyIHdheSB0byBoYW5kbGUgdGhpcyFcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLm1hcmtkb3duU2VydmljZS5oaWdobGlnaHQoKSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gbWFya2Rvd247XHJcbiAgfVxyXG59XHJcbiJdfQ==