/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { LanguagePipe } from './language.pipe';
import { MarkdownComponent } from './markdown.component';
import { MarkdownPipe } from './markdown.pipe';
import { MarkdownService } from './markdown.service';
import { MarkedOptions } from './marked-options';
/**
 * @record
 */
export function MarkdownModuleConfig() { }
function MarkdownModuleConfig_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    MarkdownModuleConfig.prototype.loader;
    /** @type {?|undefined} */
    MarkdownModuleConfig.prototype.markedOptions;
}
export var /** @type {?} */ initialMarkedOptions = {
    provide: MarkedOptions,
    useValue: {
        gfm: true,
        tables: true,
        breaks: false,
        pedantic: false,
        sanitize: false,
        smartLists: true,
        smartypants: false,
    },
};
var /** @type {?} */ sharedDeclarations = [
    LanguagePipe,
    MarkdownComponent,
    MarkdownPipe,
];
var MarkdownModule = /** @class */ (function () {
    function MarkdownModule() {
    }
    /**
     * @param {?=} markdownModuleConfig
     * @return {?}
     */
    MarkdownModule.forRoot = /**
     * @param {?=} markdownModuleConfig
     * @return {?}
     */
    function (markdownModuleConfig) {
        return {
            ngModule: MarkdownModule,
            providers: tslib_1.__spread([
                MarkdownService
            ], (markdownModuleConfig
                ? [
                    markdownModuleConfig.loader || [],
                    markdownModuleConfig.markedOptions || initialMarkedOptions,
                ]
                : [initialMarkedOptions])),
        };
    };
    /**
     * @return {?}
     */
    MarkdownModule.forChild = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: MarkdownModule,
        };
    };
    MarkdownModule.decorators = [
        { type: NgModule, args: [{
                    exports: tslib_1.__spread(sharedDeclarations),
                    declarations: tslib_1.__spread(sharedDeclarations),
                },] },
    ];
    return MarkdownModule;
}());
export { MarkdownModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2Rvd24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1hcmtkb3duLyIsInNvdXJjZXMiOlsic3JjL21hcmtkb3duLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBdUIsUUFBUSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBRXhFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQzs7Ozs7Ozs7Ozs7QUFXakQsTUFBTSxDQUFDLHFCQUFNLG9CQUFvQixHQUFhO0lBQzVDLE9BQU8sRUFBRSxhQUFhO0lBQ3RCLFFBQVEsRUFBRTtRQUNSLEdBQUcsRUFBRSxJQUFJO1FBQ1QsTUFBTSxFQUFFLElBQUk7UUFDWixNQUFNLEVBQUUsS0FBSztRQUNiLFFBQVEsRUFBRSxLQUFLO1FBQ2YsUUFBUSxFQUFFLEtBQUs7UUFDZixVQUFVLEVBQUUsSUFBSTtRQUNoQixXQUFXLEVBQUUsS0FBSztLQUNuQjtDQUNGLENBQUM7QUFFRixxQkFBTSxrQkFBa0IsR0FBRztJQUN6QixZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLFlBQVk7Q0FDYixDQUFDOzs7Ozs7OztJQVdPLHNCQUFPOzs7O0lBQWQsVUFBZSxvQkFBMkM7UUFDeEQsTUFBTSxDQUFDO1lBQ0wsUUFBUSxFQUFFLGNBQWM7WUFDeEIsU0FBUztnQkFDUCxlQUFlO2VBQ1osQ0FBQyxvQkFBb0I7Z0JBQ3RCLENBQUMsQ0FBQztvQkFDRSxvQkFBb0IsQ0FBQyxNQUFNLElBQUksRUFBRTtvQkFDakMsb0JBQW9CLENBQUMsYUFBYSxJQUFJLG9CQUFvQjtpQkFDM0Q7Z0JBQ0gsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUM1QjtTQUNGLENBQUM7S0FDSDs7OztJQUVNLHVCQUFROzs7SUFBZjtRQUNFLE1BQU0sQ0FBQztZQUNMLFFBQVEsRUFBRSxjQUFjO1NBQ3pCLENBQUM7S0FDSDs7Z0JBNUJGLFFBQVEsU0FBQztvQkFDUixPQUFPLG1CQUNGLGtCQUFrQixDQUN0QjtvQkFDRCxZQUFZLG1CQUNQLGtCQUFrQixDQUN0QjtpQkFDRjs7eUJBNUNEOztTQTZDYSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIFByb3ZpZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBMYW5ndWFnZVBpcGUgfSBmcm9tICcuL2xhbmd1YWdlLnBpcGUnO1xyXG5pbXBvcnQgeyBNYXJrZG93bkNvbXBvbmVudCB9IGZyb20gJy4vbWFya2Rvd24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWFya2Rvd25QaXBlIH0gZnJvbSAnLi9tYXJrZG93bi5waXBlJztcclxuaW1wb3J0IHsgTWFya2Rvd25TZXJ2aWNlIH0gZnJvbSAnLi9tYXJrZG93bi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWFya2VkT3B0aW9ucyB9IGZyb20gJy4vbWFya2VkLW9wdGlvbnMnO1xyXG5cclxuLy8gaGF2aW5nIGEgZGVwZW5kZW5jeSBvbiBgSHR0cENsaWVudE1vZHVsZWAgd2l0aGluIGEgbGlicmFyeVxyXG4vLyBicmVha3MgYWxsIHRoZSBpbnRlcmNlcHRvcnMgZnJvbSB0aGUgYXBwIGNvbnN1bWluZyB0aGUgbGlicmFyeVxyXG4vLyBoZXJlLCB3ZSBleHBsaWNpdGVseSBhc2sgdGhlIHVzZXIgdG8gcGFzcyBhIHByb3ZpZGVyIHdpdGhcclxuLy8gdGhlaXIgb3duIGluc3RhbmNlIG9mIGBIdHRwQ2xpZW50TW9kdWxlYFxyXG5leHBvcnQgaW50ZXJmYWNlIE1hcmtkb3duTW9kdWxlQ29uZmlnIHtcclxuICBsb2FkZXI/OiBQcm92aWRlcjtcclxuICBtYXJrZWRPcHRpb25zPzogUHJvdmlkZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBpbml0aWFsTWFya2VkT3B0aW9uczogUHJvdmlkZXIgPSB7XHJcbiAgcHJvdmlkZTogTWFya2VkT3B0aW9ucyxcclxuICB1c2VWYWx1ZToge1xyXG4gICAgZ2ZtOiB0cnVlLFxyXG4gICAgdGFibGVzOiB0cnVlLFxyXG4gICAgYnJlYWtzOiBmYWxzZSxcclxuICAgIHBlZGFudGljOiBmYWxzZSxcclxuICAgIHNhbml0aXplOiBmYWxzZSxcclxuICAgIHNtYXJ0TGlzdHM6IHRydWUsXHJcbiAgICBzbWFydHlwYW50czogZmFsc2UsXHJcbiAgfSxcclxufTtcclxuXHJcbmNvbnN0IHNoYXJlZERlY2xhcmF0aW9ucyA9IFtcclxuICBMYW5ndWFnZVBpcGUsXHJcbiAgTWFya2Rvd25Db21wb25lbnQsXHJcbiAgTWFya2Rvd25QaXBlLFxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBleHBvcnRzOiBbXHJcbiAgICAuLi5zaGFyZWREZWNsYXJhdGlvbnMsXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIC4uLnNoYXJlZERlY2xhcmF0aW9ucyxcclxuICBdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFya2Rvd25Nb2R1bGUge1xyXG4gIHN0YXRpYyBmb3JSb290KG1hcmtkb3duTW9kdWxlQ29uZmlnPzogTWFya2Rvd25Nb2R1bGVDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBNYXJrZG93bk1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgTWFya2Rvd25TZXJ2aWNlLFxyXG4gICAgICAgIC4uLihtYXJrZG93bk1vZHVsZUNvbmZpZ1xyXG4gICAgICAgICAgPyBbXHJcbiAgICAgICAgICAgICAgbWFya2Rvd25Nb2R1bGVDb25maWcubG9hZGVyIHx8IFtdLFxyXG4gICAgICAgICAgICAgIG1hcmtkb3duTW9kdWxlQ29uZmlnLm1hcmtlZE9wdGlvbnMgfHwgaW5pdGlhbE1hcmtlZE9wdGlvbnMsXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICAgIDogW2luaXRpYWxNYXJrZWRPcHRpb25zXSksXHJcbiAgICAgIF0sXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIGZvckNoaWxkKCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IE1hcmtkb3duTW9kdWxlLFxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuIl19