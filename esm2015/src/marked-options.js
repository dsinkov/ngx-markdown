/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
export class MarkedOptions {
}
function MarkedOptions_tsickle_Closure_declarations() {
    /**
     * Type: object Default: new Renderer()
     *
     * An object containing functions to render tokens to HTML.
     * @type {?}
     */
    MarkedOptions.prototype.renderer;
    /**
     * Enable GitHub flavored markdown.
     * @type {?}
     */
    MarkedOptions.prototype.gfm;
    /**
     * Enable GFM tables. This option requires the gfm option to be true.
     * @type {?}
     */
    MarkedOptions.prototype.tables;
    /**
     * Enable GFM line breaks. This option requires the gfm option to be true.
     * @type {?}
     */
    MarkedOptions.prototype.breaks;
    /**
     * Conform to obscure parts of markdown.pl as much as possible. Don't fix any of the original markdown bugs or poor behavior.
     * @type {?}
     */
    MarkedOptions.prototype.pedantic;
    /**
     * Sanitize the output. Ignore any HTML that has been input.
     * @type {?}
     */
    MarkedOptions.prototype.sanitize;
    /**
     * Mangle autolinks (<email\@domain.com>).
     * @type {?}
     */
    MarkedOptions.prototype.mangle;
    /**
     * Use smarter list behavior than the original markdown. May eventually be default with the old behavior moved into pedantic.
     * @type {?}
     */
    MarkedOptions.prototype.smartLists;
    /**
     * Shows an HTML error message when rendering fails.
     * @type {?}
     */
    MarkedOptions.prototype.silent;
    /**
     * Set the prefix for code block classes.
     * @type {?}
     */
    MarkedOptions.prototype.langPrefix;
    /**
     * Use "smart" typograhic punctuation for things like quotes and dashes.
     * @type {?}
     */
    MarkedOptions.prototype.smartypants;
    /**
     * Set the prefix for header tag ids.
     * @type {?}
     */
    MarkedOptions.prototype.headerPrefix;
    /**
     * Generate closing slash for self-closing tags (<br/> instead of <br>)
     * @type {?}
     */
    MarkedOptions.prototype.xhtml;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2VkLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFya2Rvd24vIiwic291cmNlcyI6WyJzcmMvbWFya2VkLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBLE1BQU07Q0E2RUwiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZW5kZXJlciB9IGZyb20gJ21hcmtlZCc7XHJcblxyXG5leHBvcnQgY2xhc3MgTWFya2VkT3B0aW9ucyBpbXBsZW1lbnRzIG1hcmtlZC5NYXJrZWRPcHRpb25zIHtcclxuICAvKipcclxuICAgKiBUeXBlOiBvYmplY3QgRGVmYXVsdDogbmV3IFJlbmRlcmVyKClcclxuICAgKlxyXG4gICAqIEFuIG9iamVjdCBjb250YWluaW5nIGZ1bmN0aW9ucyB0byByZW5kZXIgdG9rZW5zIHRvIEhUTUwuXHJcbiAgICovXHJcbiAgcmVuZGVyZXI/OiBSZW5kZXJlcjtcclxuXHJcbiAgLyoqXHJcbiAgICogRW5hYmxlIEdpdEh1YiBmbGF2b3JlZCBtYXJrZG93bi5cclxuICAgKi9cclxuICBnZm0/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBFbmFibGUgR0ZNIHRhYmxlcy4gVGhpcyBvcHRpb24gcmVxdWlyZXMgdGhlIGdmbSBvcHRpb24gdG8gYmUgdHJ1ZS5cclxuICAgKi9cclxuICB0YWJsZXM/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBFbmFibGUgR0ZNIGxpbmUgYnJlYWtzLiBUaGlzIG9wdGlvbiByZXF1aXJlcyB0aGUgZ2ZtIG9wdGlvbiB0byBiZSB0cnVlLlxyXG4gICAqL1xyXG4gIGJyZWFrcz86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbmZvcm0gdG8gb2JzY3VyZSBwYXJ0cyBvZiBtYXJrZG93bi5wbCBhcyBtdWNoIGFzIHBvc3NpYmxlLiBEb24ndCBmaXggYW55IG9mIHRoZSBvcmlnaW5hbCBtYXJrZG93biBidWdzIG9yIHBvb3IgYmVoYXZpb3IuXHJcbiAgICovXHJcbiAgcGVkYW50aWM/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBTYW5pdGl6ZSB0aGUgb3V0cHV0LiBJZ25vcmUgYW55IEhUTUwgdGhhdCBoYXMgYmVlbiBpbnB1dC5cclxuICAgKi9cclxuICBzYW5pdGl6ZT86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIE1hbmdsZSBhdXRvbGlua3MgKDxlbWFpbEBkb21haW4uY29tPikuXHJcbiAgICovXHJcbiAgbWFuZ2xlPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogVXNlIHNtYXJ0ZXIgbGlzdCBiZWhhdmlvciB0aGFuIHRoZSBvcmlnaW5hbCBtYXJrZG93bi4gTWF5IGV2ZW50dWFsbHkgYmUgZGVmYXVsdCB3aXRoIHRoZSBvbGQgYmVoYXZpb3IgbW92ZWQgaW50byBwZWRhbnRpYy5cclxuICAgKi9cclxuICBzbWFydExpc3RzPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogU2hvd3MgYW4gSFRNTCBlcnJvciBtZXNzYWdlIHdoZW4gcmVuZGVyaW5nIGZhaWxzLlxyXG4gICAqL1xyXG4gIHNpbGVudD86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldCB0aGUgcHJlZml4IGZvciBjb2RlIGJsb2NrIGNsYXNzZXMuXHJcbiAgICovXHJcbiAgbGFuZ1ByZWZpeD86IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogVXNlIFwic21hcnRcIiB0eXBvZ3JhaGljIHB1bmN0dWF0aW9uIGZvciB0aGluZ3MgbGlrZSBxdW90ZXMgYW5kIGRhc2hlcy5cclxuICAgKi9cclxuICBzbWFydHlwYW50cz86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldCB0aGUgcHJlZml4IGZvciBoZWFkZXIgdGFnIGlkcy5cclxuICAgKi9cclxuICBoZWFkZXJQcmVmaXg/OiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEdlbmVyYXRlIGNsb3Npbmcgc2xhc2ggZm9yIHNlbGYtY2xvc2luZyB0YWdzICg8YnIvPiBpbnN0ZWFkIG9mIDxicj4pXHJcbiAgICovXHJcbiAgeGh0bWw/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBBIGZ1bmN0aW9uIHRvIGhpZ2hsaWdodCBjb2RlIGJsb2Nrcy4gVGhlIGZ1bmN0aW9uIHRha2VzIHRocmVlIGFyZ3VtZW50czogY29kZSwgbGFuZywgYW5kIGNhbGxiYWNrLlxyXG4gICAqL1xyXG4gIGhpZ2hsaWdodD8oY29kZTogc3RyaW5nLCBsYW5nOiBzdHJpbmcsIGNhbGxiYWNrPzogKGVycm9yOiBhbnkgfCB1bmRlZmluZWQsIGNvZGU6IHN0cmluZykgPT4gdm9pZCk6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogT3B0aW9uYWxseSBzYW5pdGl6ZSBmb3VuZCBIVE1MIHdpdGggYSBzYW5pdGl6ZXIgZnVuY3Rpb24uXHJcbiAgICovXHJcbiAgc2FuaXRpemVyPyhodG1sOiBzdHJpbmcpOiBzdHJpbmc7XHJcbn1cclxuIl19