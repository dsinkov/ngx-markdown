/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { parse, Renderer } from 'marked';
import { map } from 'rxjs/operators';
import { MarkedOptions } from './marked-options';
// tslint:disable-next-line:max-line-length
export const /** @type {?} */ errorSrcWithoutHttpClient = '[ngx-markdown] When using the [src] attribute you *have to* pass the `HttpClient` as a parameter of the `forRoot` method. See README for more information';
export class MarkdownService {
    /**
     * @param {?} http
     * @param {?} options
     */
    constructor(http, options) {
        this.http = http;
        this.options = options;
        if (!this.renderer) {
            this.renderer = new Renderer();
        }
    }
    /**
     * @return {?}
     */
    get renderer() {
        return this.options.renderer;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set renderer(value) {
        this.options.renderer = value;
    }
    /**
     * @param {?} markdown
     * @param {?=} markedOptions
     * @return {?}
     */
    compile(markdown, markedOptions = this.options) {
        const /** @type {?} */ precompiled = this.precompile(markdown);
        return parse(precompiled, markedOptions);
    }
    /**
     * @param {?} src
     * @return {?}
     */
    getSource(src) {
        if (!this.http) {
            throw new Error(errorSrcWithoutHttpClient);
        }
        return this.http
            .get(src, { responseType: 'text' })
            .pipe(map(markdown => this.handleExtension(src, markdown)));
    }
    /**
     * @return {?}
     */
    highlight() {
        if (typeof Prism !== 'undefined') {
            Prism.highlightAll(false);
        }
    }
    /**
     * @param {?} src
     * @param {?} markdown
     * @return {?}
     */
    handleExtension(src, markdown) {
        const /** @type {?} */ extension = src
            ? src.split('.').splice(-1).join()
            : null;
        return extension !== 'md'
            ? '```' + extension + '\n' + markdown + '\n```'
            : markdown;
    }
    /**
     * @param {?} markdown
     * @return {?}
     */
    precompile(markdown) {
        if (!markdown) {
            return '';
        }
        let /** @type {?} */ indentStart;
        return markdown
            .replace(/\&gt;/g, '>')
            .split('\n')
            .map(line => {
            // find position of 1st non-whitespace character
            // to determine the markdown indentation start
            if (line.length > 0 && isNaN(indentStart)) {
                indentStart = line.search(/\S|$/);
            }
            // remove whitespaces before indentation start
            return indentStart
                ? line.substring(indentStart)
                : line;
        }).join('\n');
    }
}
MarkdownService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MarkdownService.ctorParameters = () => [
    { type: HttpClient, decorators: [{ type: Optional }] },
    { type: MarkedOptions }
];
function MarkdownService_tsickle_Closure_declarations() {
    /** @type {?} */
    MarkdownService.prototype.http;
    /** @type {?} */
    MarkdownService.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2Rvd24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYXJrZG93bi8iLCJzb3VyY2VzIjpbInNyYy9tYXJrZG93bi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFVLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFekMsT0FBTyxFQUFjLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWpELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQzs7QUFRakQsTUFBTSxDQUFDLHVCQUFNLHlCQUF5QixHQUFHLDJKQUEySixDQUFDO0FBR3JNLE1BQU07Ozs7O0lBUUosWUFDc0IsSUFBZ0IsRUFDN0I7UUFEYSxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQzdCLFlBQU8sR0FBUCxPQUFPO1FBRWQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7U0FDaEM7S0FDRjs7OztJQWRELElBQUksUUFBUTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztLQUM5Qjs7Ozs7SUFDRCxJQUFJLFFBQVEsQ0FBQyxLQUFzQjtRQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7S0FDL0I7Ozs7OztJQVdELE9BQU8sQ0FBQyxRQUFnQixFQUFFLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTztRQUNwRCx1QkFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQztLQUMxQzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBVztRQUNuQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2YsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsQ0FBQzthQUNsQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQy9EOzs7O0lBRUQsU0FBUztRQUNQLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDakMsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjtLQUNGOzs7Ozs7SUFFTyxlQUFlLENBQUMsR0FBVyxFQUFFLFFBQWdCO1FBQ25ELHVCQUFNLFNBQVMsR0FBRyxHQUFHO1lBQ25CLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtZQUNsQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ1QsTUFBTSxDQUFDLFNBQVMsS0FBSyxJQUFJO1lBQ3ZCLENBQUMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxHQUFHLElBQUksR0FBRyxRQUFRLEdBQUcsT0FBTztZQUMvQyxDQUFDLENBQUMsUUFBUSxDQUFDOzs7Ozs7SUFHUCxVQUFVLENBQUMsUUFBZ0I7UUFDakMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2QsTUFBTSxDQUFDLEVBQUUsQ0FBQztTQUNYO1FBQ0QscUJBQUksV0FBbUIsQ0FBQztRQUN4QixNQUFNLENBQUMsUUFBUTthQUNaLE9BQU8sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDO2FBQ3RCLEtBQUssQ0FBQyxJQUFJLENBQUM7YUFDWCxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7OztZQUdWLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ25DOztZQUVELE1BQU0sQ0FBQyxXQUFXO2dCQUNoQixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDVixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzs7O1lBbEVuQixVQUFVOzs7O1lBaEJGLFVBQVUsdUJBMEJkLFFBQVE7WUFwQkosYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgcGFyc2UsIFJlbmRlcmVyIH0gZnJvbSAnbWFya2VkJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBNYXJrZWRPcHRpb25zIH0gZnJvbSAnLi9tYXJrZWQtb3B0aW9ucyc7XHJcbmltcG9ydCB7IE1hcmtlZFJlbmRlcmVyIH0gZnJvbSAnLi9tYXJrZWQtcmVuZGVyZXInO1xyXG5cclxuZGVjbGFyZSB2YXIgUHJpc206IHtcclxuICBoaWdobGlnaHRBbGw6IChhc3luYzogYm9vbGVhbikgPT4gdm9pZDtcclxufTtcclxuXHJcbi8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcclxuZXhwb3J0IGNvbnN0IGVycm9yU3JjV2l0aG91dEh0dHBDbGllbnQgPSAnW25neC1tYXJrZG93bl0gV2hlbiB1c2luZyB0aGUgW3NyY10gYXR0cmlidXRlIHlvdSAqaGF2ZSB0byogcGFzcyB0aGUgYEh0dHBDbGllbnRgIGFzIGEgcGFyYW1ldGVyIG9mIHRoZSBgZm9yUm9vdGAgbWV0aG9kLiBTZWUgUkVBRE1FIGZvciBtb3JlIGluZm9ybWF0aW9uJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1hcmtkb3duU2VydmljZSB7XHJcbiAgZ2V0IHJlbmRlcmVyKCk6IFJlbmRlcmVyIHtcclxuICAgIHJldHVybiB0aGlzLm9wdGlvbnMucmVuZGVyZXI7XHJcbiAgfVxyXG4gIHNldCByZW5kZXJlcih2YWx1ZTogbWFya2VkLlJlbmRlcmVyKSB7XHJcbiAgICB0aGlzLm9wdGlvbnMucmVuZGVyZXIgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgQE9wdGlvbmFsKCkgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgcHVibGljIG9wdGlvbnM6IE1hcmtlZE9wdGlvbnMsXHJcbiAgKSB7XHJcbiAgICBpZiAoIXRoaXMucmVuZGVyZXIpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlciA9IG5ldyBSZW5kZXJlcigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29tcGlsZShtYXJrZG93bjogc3RyaW5nLCBtYXJrZWRPcHRpb25zID0gdGhpcy5vcHRpb25zKSB7XHJcbiAgICBjb25zdCBwcmVjb21waWxlZCA9IHRoaXMucHJlY29tcGlsZShtYXJrZG93bik7XHJcbiAgICByZXR1cm4gcGFyc2UocHJlY29tcGlsZWQsIG1hcmtlZE9wdGlvbnMpO1xyXG4gIH1cclxuXHJcbiAgZ2V0U291cmNlKHNyYzogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMuaHR0cCkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3JTcmNXaXRob3V0SHR0cENsaWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cFxyXG4gICAgICAuZ2V0KHNyYywgeyByZXNwb25zZVR5cGU6ICd0ZXh0JyB9KVxyXG4gICAgICAucGlwZShtYXAobWFya2Rvd24gPT4gdGhpcy5oYW5kbGVFeHRlbnNpb24oc3JjLCBtYXJrZG93bikpKTtcclxuICB9XHJcblxyXG4gIGhpZ2hsaWdodCgpIHtcclxuICAgIGlmICh0eXBlb2YgUHJpc20gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgIFByaXNtLmhpZ2hsaWdodEFsbChmYWxzZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUV4dGVuc2lvbihzcmM6IHN0cmluZywgbWFya2Rvd246IHN0cmluZykge1xyXG4gICAgY29uc3QgZXh0ZW5zaW9uID0gc3JjXHJcbiAgICAgID8gc3JjLnNwbGl0KCcuJykuc3BsaWNlKC0xKS5qb2luKClcclxuICAgICAgOiBudWxsO1xyXG4gICAgcmV0dXJuIGV4dGVuc2lvbiAhPT0gJ21kJ1xyXG4gICAgICA/ICdgYGAnICsgZXh0ZW5zaW9uICsgJ1xcbicgKyBtYXJrZG93biArICdcXG5gYGAnXHJcbiAgICAgIDogbWFya2Rvd247XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZWNvbXBpbGUobWFya2Rvd246IHN0cmluZykge1xyXG4gICAgaWYgKCFtYXJrZG93bikge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbiAgICBsZXQgaW5kZW50U3RhcnQ6IG51bWJlcjtcclxuICAgIHJldHVybiBtYXJrZG93blxyXG4gICAgICAucmVwbGFjZSgvXFwmZ3Q7L2csICc+JylcclxuICAgICAgLnNwbGl0KCdcXG4nKVxyXG4gICAgICAubWFwKGxpbmUgPT4ge1xyXG4gICAgICAgIC8vIGZpbmQgcG9zaXRpb24gb2YgMXN0IG5vbi13aGl0ZXNwYWNlIGNoYXJhY3RlclxyXG4gICAgICAgIC8vIHRvIGRldGVybWluZSB0aGUgbWFya2Rvd24gaW5kZW50YXRpb24gc3RhcnRcclxuICAgICAgICBpZiAobGluZS5sZW5ndGggPiAwICYmIGlzTmFOKGluZGVudFN0YXJ0KSkge1xyXG4gICAgICAgICAgaW5kZW50U3RhcnQgPSBsaW5lLnNlYXJjaCgvXFxTfCQvKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gcmVtb3ZlIHdoaXRlc3BhY2VzIGJlZm9yZSBpbmRlbnRhdGlvbiBzdGFydFxyXG4gICAgICAgIHJldHVybiBpbmRlbnRTdGFydFxyXG4gICAgICAgICAgPyBsaW5lLnN1YnN0cmluZyhpbmRlbnRTdGFydClcclxuICAgICAgICAgIDogbGluZTtcclxuICAgICAgfSkuam9pbignXFxuJyk7XHJcbiAgfVxyXG59XHJcbiJdfQ==