(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('marked'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('ngx-markdown', ['exports', '@angular/core', '@angular/common/http', 'marked', 'rxjs/operators'], factory) :
    (factory((global['ngx-markdown'] = {}),global.ng.core,global.ng.common.http,null,global.rxjs.operators));
}(this, (function (exports,core,http,marked,operators) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LanguagePipe = (function () {
        function LanguagePipe() {
        }
        /**
         * @param {?} value
         * @param {?} language
         * @return {?}
         */
        LanguagePipe.prototype.transform = /**
         * @param {?} value
         * @param {?} language
         * @return {?}
         */
            function (value, language) {
                if (typeof value !== 'string') {
                    console.error("LanguagePipe has been invoked with an invalid value type [" + value + "]");
                    return value;
                }
                if (typeof language !== 'string') {
                    console.error("LanguagePipe has been invoked with an invalid parameter [" + language + "]");
                    return value;
                }
                return '```' + language + '\n' + value + '\n```';
            };
        LanguagePipe.decorators = [
            { type: core.Pipe, args: [{
                        name: 'language',
                    },] },
        ];
        return LanguagePipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MarkedOptions = (function () {
        function MarkedOptions() {
        }
        return MarkedOptions;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    // tslint:disable-next-line:max-line-length
    var /** @type {?} */ errorSrcWithoutHttpClient = '[ngx-markdown] When using the [src] attribute you *have to* pass the `HttpClient` as a parameter of the `forRoot` method. See README for more information';
    var MarkdownService = (function () {
        function MarkdownService(http$$1, options) {
            this.http = http$$1;
            this.options = options;
            if (!this.renderer) {
                this.renderer = new marked.Renderer();
            }
        }
        Object.defineProperty(MarkdownService.prototype, "renderer", {
            get: /**
             * @return {?}
             */ function () {
                return this.options.renderer;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this.options.renderer = value;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} markdown
         * @param {?=} markedOptions
         * @return {?}
         */
        MarkdownService.prototype.compile = /**
         * @param {?} markdown
         * @param {?=} markedOptions
         * @return {?}
         */
            function (markdown, markedOptions) {
                if (markedOptions === void 0) {
                    markedOptions = this.options;
                }
                var /** @type {?} */ precompiled = this.precompile(markdown);
                return marked.parse(precompiled, markedOptions);
            };
        /**
         * @param {?} src
         * @return {?}
         */
        MarkdownService.prototype.getSource = /**
         * @param {?} src
         * @return {?}
         */
            function (src) {
                var _this = this;
                if (!this.http) {
                    throw new Error(errorSrcWithoutHttpClient);
                }
                return this.http
                    .get(src, { responseType: 'text' })
                    .pipe(operators.map(function (markdown) { return _this.handleExtension(src, markdown); }));
            };
        /**
         * @return {?}
         */
        MarkdownService.prototype.highlight = /**
         * @return {?}
         */
            function () {
                if (typeof Prism !== 'undefined') {
                    Prism.highlightAll(false);
                }
            };
        /**
         * @param {?} src
         * @param {?} markdown
         * @return {?}
         */
        MarkdownService.prototype.handleExtension = /**
         * @param {?} src
         * @param {?} markdown
         * @return {?}
         */
            function (src, markdown) {
                var /** @type {?} */ extension = src
                    ? src.split('.').splice(-1).join()
                    : null;
                return extension !== 'md'
                    ? '```' + extension + '\n' + markdown + '\n```'
                    : markdown;
            };
        /**
         * @param {?} markdown
         * @return {?}
         */
        MarkdownService.prototype.precompile = /**
         * @param {?} markdown
         * @return {?}
         */
            function (markdown) {
                if (!markdown) {
                    return '';
                }
                var /** @type {?} */ indentStart;
                return markdown
                    .replace(/\&gt;/g, '>')
                    .split('\n')
                    .map(function (line) {
                    // find position of 1st non-whitespace character
                    // to determine the markdown indentation start
                    if (line.length > 0 && isNaN(indentStart)) {
                        indentStart = line.search(/\S|$/);
                    }
                    // remove whitespaces before indentation start
                    return indentStart
                        ? line.substring(indentStart)
                        : line;
                }).join('\n');
            };
        MarkdownService.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        MarkdownService.ctorParameters = function () {
            return [
                { type: http.HttpClient, decorators: [{ type: core.Optional }] },
                { type: MarkedOptions }
            ];
        };
        return MarkdownService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MarkdownComponent = (function () {
        function MarkdownComponent(element, markdownService) {
            this.element = element;
            this.markdownService = markdownService;
            this.isTargetBlankLinks = false;
            this.error = new core.EventEmitter();
            this.load = new core.EventEmitter();
        }
        Object.defineProperty(MarkdownComponent.prototype, "data", {
            get: /**
             * @return {?}
             */ function () {
                return this._data;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._data = value;
                this.render(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MarkdownComponent.prototype, "src", {
            get: /**
             * @return {?}
             */ function () {
                return this._src;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                var _this = this;
                this._src = value;
                this.markdownService
                    .getSource(value)
                    .subscribe(function (markdown) {
                    _this.render(markdown);
                    _this.load.emit(markdown);
                }, function (error) { return _this.error.emit(error); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MarkdownComponent.prototype, "isTranscluded", {
            get: /**
             * @return {?}
             */ function () {
                return !this.data && !this.src;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} href
         * @param {?} title
         * @param {?} text
         * @return {?}
         */
        MarkdownComponent.addTargetBlank = /**
         * @param {?} href
         * @param {?} title
         * @param {?} text
         * @return {?}
         */
            function (href, title, text) {
                var /** @type {?} */ out;
                out = '<a href="' + href + '"';
                out += ' target="_blank"';
                if (title) {
                    out += ' title="' + title + '"';
                }
                return out + '>' + text + '</a>';
            };
        /**
         * @return {?}
         */
        MarkdownComponent.prototype.ngAfterViewInit = /**
         * @return {?}
         */
            function () {
                if (this.isTargetBlankLinks) {
                    var /** @type {?} */ customRenderer = new marked.Renderer();
                    customRenderer.link = MarkdownComponent.addTargetBlank;
                    this.markdownService.renderer = customRenderer;
                }
                if (this.isTranscluded) {
                    this.render(this.element.nativeElement.innerHTML);
                }
            };
        /**
         * @param {?} markdown
         * @return {?}
         */
        MarkdownComponent.prototype.render = /**
         * @param {?} markdown
         * @return {?}
         */
            function (markdown) {
                this.element.nativeElement.innerHTML = this.markdownService.compile(markdown);
                this.markdownService.highlight();
            };
        MarkdownComponent.decorators = [
            { type: core.Component, args: [{
                        // tslint:disable-next-line:component-selector
                        selector: 'markdown, [markdown]',
                        template: '<ng-content></ng-content>',
                        styles: [":host /deep/ table{border-spacing:0;border-collapse:collapse;margin-bottom:16px}:host /deep/ table td,:host /deep/ table th{padding:6px 13px;border:1px solid #ddd}:host /deep/ table td[align=left],:host /deep/ table th[align=left]{text-align:left}:host /deep/ table td[align=center],:host /deep/ table th[align=center]{text-align:center}:host /deep/ table td[align=right],:host /deep/ table th[align=right]{text-align:right}:host /deep/ table tr:nth-child(2n){background-color:rgba(0,0,0,.03)}:host /deep/ blockquote{padding:0 1em;color:rgba(0,0,0,.535);border-left:.25em solid rgba(0,0,0,.11)}"],
                        preserveWhitespaces: true,
                    },] },
        ];
        /** @nocollapse */
        MarkdownComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef },
                { type: MarkdownService }
            ];
        };
        MarkdownComponent.propDecorators = {
            data: [{ type: core.Input }],
            src: [{ type: core.Input }],
            isTargetBlankLinks: [{ type: core.Input }],
            error: [{ type: core.Output }],
            load: [{ type: core.Output }]
        };
        return MarkdownComponent;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b)
            if (b.hasOwnProperty(p))
                d[p] = b[p]; };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MarkdownPipe = (function () {
        function MarkdownPipe(markdownService, zone) {
            this.markdownService = markdownService;
            this.zone = zone;
        }
        /**
         * @param {?} value
         * @return {?}
         */
        MarkdownPipe.prototype.transform = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                var _this = this;
                if (typeof value !== 'string') {
                    console.error("MarkdownPipe has been invoked with an invalid value type [" + value + "]");
                    return value;
                }
                var /** @type {?} */ markdown = this.markdownService.compile(value);
                this.zone.runOutsideAngular(function () {
                    // glitch in the UI... need a better way to handle this!
                    setTimeout(function () { return _this.markdownService.highlight(); });
                });
                return markdown;
            };
        MarkdownPipe.decorators = [
            { type: core.Pipe, args: [{
                        name: 'markdown',
                    },] },
        ];
        /** @nocollapse */
        MarkdownPipe.ctorParameters = function () {
            return [
                { type: MarkdownService },
                { type: core.NgZone }
            ];
        };
        return MarkdownPipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ initialMarkedOptions = {
        provide: MarkedOptions,
        useValue: {
            gfm: true,
            tables: true,
            breaks: false,
            pedantic: false,
            sanitize: false,
            smartLists: true,
            smartypants: false,
        },
    };
    var /** @type {?} */ sharedDeclarations = [
        LanguagePipe,
        MarkdownComponent,
        MarkdownPipe,
    ];
    var MarkdownModule = (function () {
        function MarkdownModule() {
        }
        /**
         * @param {?=} markdownModuleConfig
         * @return {?}
         */
        MarkdownModule.forRoot = /**
         * @param {?=} markdownModuleConfig
         * @return {?}
         */
            function (markdownModuleConfig) {
                return {
                    ngModule: MarkdownModule,
                    providers: __spread([
                        MarkdownService
                    ], (markdownModuleConfig
                        ? [
                            markdownModuleConfig.loader || [],
                            markdownModuleConfig.markedOptions || initialMarkedOptions,
                        ]
                        : [initialMarkedOptions])),
                };
            };
        /**
         * @return {?}
         */
        MarkdownModule.forChild = /**
         * @return {?}
         */
            function () {
                return {
                    ngModule: MarkdownModule,
                };
            };
        MarkdownModule.decorators = [
            { type: core.NgModule, args: [{
                        exports: __spread(sharedDeclarations),
                        declarations: __spread(sharedDeclarations),
                    },] },
        ];
        return MarkdownModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MarkedRenderer = (function (_super) {
        __extends(MarkedRenderer, _super);
        function MarkedRenderer() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return MarkedRenderer;
    }(marked.Renderer));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.LanguagePipe = LanguagePipe;
    exports.MarkdownComponent = MarkdownComponent;
    exports.initialMarkedOptions = initialMarkedOptions;
    exports.MarkdownModule = MarkdownModule;
    exports.MarkdownPipe = MarkdownPipe;
    exports.errorSrcWithoutHttpClient = errorSrcWithoutHttpClient;
    exports.MarkdownService = MarkdownService;
    exports.MarkedOptions = MarkedOptions;
    exports.MarkedRenderer = MarkedRenderer;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LW1hcmtkb3duLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vbmd4LW1hcmtkb3duL3NyYy9sYW5ndWFnZS5waXBlLnRzIiwibmc6Ly9uZ3gtbWFya2Rvd24vc3JjL21hcmtlZC1vcHRpb25zLnRzIiwibmc6Ly9uZ3gtbWFya2Rvd24vc3JjL21hcmtkb3duLnNlcnZpY2UudHMiLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2Rvd24uY29tcG9uZW50LnRzIixudWxsLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2Rvd24ucGlwZS50cyIsIm5nOi8vbmd4LW1hcmtkb3duL3NyYy9tYXJrZG93bi5tb2R1bGUudHMiLCJuZzovL25neC1tYXJrZG93bi9zcmMvbWFya2VkLXJlbmRlcmVyLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnbGFuZ3VhZ2UnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFuZ3VhZ2VQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCBsYW5ndWFnZTogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoYExhbmd1YWdlUGlwZSBoYXMgYmVlbiBpbnZva2VkIHdpdGggYW4gaW52YWxpZCB2YWx1ZSB0eXBlIFske3ZhbHVlfV1gKTtcclxuICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiBsYW5ndWFnZSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgY29uc29sZS5lcnJvcihgTGFuZ3VhZ2VQaXBlIGhhcyBiZWVuIGludm9rZWQgd2l0aCBhbiBpbnZhbGlkIHBhcmFtZXRlciBbJHtsYW5ndWFnZX1dYCk7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiAnYGBgJyArIGxhbmd1YWdlICsgJ1xcbicgKyAgdmFsdWUgKyAnXFxuYGBgJztcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgUmVuZGVyZXIgfSBmcm9tICdtYXJrZWQnO1xyXG5cclxuZXhwb3J0IGNsYXNzIE1hcmtlZE9wdGlvbnMgaW1wbGVtZW50cyBtYXJrZWQuTWFya2VkT3B0aW9ucyB7XHJcbiAgLyoqXHJcbiAgICogVHlwZTogb2JqZWN0IERlZmF1bHQ6IG5ldyBSZW5kZXJlcigpXHJcbiAgICpcclxuICAgKiBBbiBvYmplY3QgY29udGFpbmluZyBmdW5jdGlvbnMgdG8gcmVuZGVyIHRva2VucyB0byBIVE1MLlxyXG4gICAqL1xyXG4gIHJlbmRlcmVyPzogUmVuZGVyZXI7XHJcblxyXG4gIC8qKlxyXG4gICAqIEVuYWJsZSBHaXRIdWIgZmxhdm9yZWQgbWFya2Rvd24uXHJcbiAgICovXHJcbiAgZ2ZtPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogRW5hYmxlIEdGTSB0YWJsZXMuIFRoaXMgb3B0aW9uIHJlcXVpcmVzIHRoZSBnZm0gb3B0aW9uIHRvIGJlIHRydWUuXHJcbiAgICovXHJcbiAgdGFibGVzPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogRW5hYmxlIEdGTSBsaW5lIGJyZWFrcy4gVGhpcyBvcHRpb24gcmVxdWlyZXMgdGhlIGdmbSBvcHRpb24gdG8gYmUgdHJ1ZS5cclxuICAgKi9cclxuICBicmVha3M/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBDb25mb3JtIHRvIG9ic2N1cmUgcGFydHMgb2YgbWFya2Rvd24ucGwgYXMgbXVjaCBhcyBwb3NzaWJsZS4gRG9uJ3QgZml4IGFueSBvZiB0aGUgb3JpZ2luYWwgbWFya2Rvd24gYnVncyBvciBwb29yIGJlaGF2aW9yLlxyXG4gICAqL1xyXG4gIHBlZGFudGljPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogU2FuaXRpemUgdGhlIG91dHB1dC4gSWdub3JlIGFueSBIVE1MIHRoYXQgaGFzIGJlZW4gaW5wdXQuXHJcbiAgICovXHJcbiAgc2FuaXRpemU/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBNYW5nbGUgYXV0b2xpbmtzICg8ZW1haWxAZG9tYWluLmNvbT4pLlxyXG4gICAqL1xyXG4gIG1hbmdsZT86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFVzZSBzbWFydGVyIGxpc3QgYmVoYXZpb3IgdGhhbiB0aGUgb3JpZ2luYWwgbWFya2Rvd24uIE1heSBldmVudHVhbGx5IGJlIGRlZmF1bHQgd2l0aCB0aGUgb2xkIGJlaGF2aW9yIG1vdmVkIGludG8gcGVkYW50aWMuXHJcbiAgICovXHJcbiAgc21hcnRMaXN0cz86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFNob3dzIGFuIEhUTUwgZXJyb3IgbWVzc2FnZSB3aGVuIHJlbmRlcmluZyBmYWlscy5cclxuICAgKi9cclxuICBzaWxlbnQ/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIHByZWZpeCBmb3IgY29kZSBibG9jayBjbGFzc2VzLlxyXG4gICAqL1xyXG4gIGxhbmdQcmVmaXg/OiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFVzZSBcInNtYXJ0XCIgdHlwb2dyYWhpYyBwdW5jdHVhdGlvbiBmb3IgdGhpbmdzIGxpa2UgcXVvdGVzIGFuZCBkYXNoZXMuXHJcbiAgICovXHJcbiAgc21hcnR5cGFudHM/OiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIHByZWZpeCBmb3IgaGVhZGVyIHRhZyBpZHMuXHJcbiAgICovXHJcbiAgaGVhZGVyUHJlZml4Pzogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBHZW5lcmF0ZSBjbG9zaW5nIHNsYXNoIGZvciBzZWxmLWNsb3NpbmcgdGFncyAoPGJyLz4gaW5zdGVhZCBvZiA8YnI+KVxyXG4gICAqL1xyXG4gIHhodG1sPzogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogQSBmdW5jdGlvbiB0byBoaWdobGlnaHQgY29kZSBibG9ja3MuIFRoZSBmdW5jdGlvbiB0YWtlcyB0aHJlZSBhcmd1bWVudHM6IGNvZGUsIGxhbmcsIGFuZCBjYWxsYmFjay5cclxuICAgKi9cclxuICBoaWdobGlnaHQ/KGNvZGU6IHN0cmluZywgbGFuZzogc3RyaW5nLCBjYWxsYmFjaz86IChlcnJvcjogYW55IHwgdW5kZWZpbmVkLCBjb2RlOiBzdHJpbmcpID0+IHZvaWQpOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIE9wdGlvbmFsbHkgc2FuaXRpemUgZm91bmQgSFRNTCB3aXRoIGEgc2FuaXRpemVyIGZ1bmN0aW9uLlxyXG4gICAqL1xyXG4gIHNhbml0aXplcj8oaHRtbDogc3RyaW5nKTogc3RyaW5nO1xyXG59XHJcbiIsImltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgcGFyc2UsIFJlbmRlcmVyIH0gZnJvbSAnbWFya2VkJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBNYXJrZWRPcHRpb25zIH0gZnJvbSAnLi9tYXJrZWQtb3B0aW9ucyc7XHJcbmltcG9ydCB7IE1hcmtlZFJlbmRlcmVyIH0gZnJvbSAnLi9tYXJrZWQtcmVuZGVyZXInO1xyXG5cclxuZGVjbGFyZSB2YXIgUHJpc206IHtcclxuICBoaWdobGlnaHRBbGw6IChhc3luYzogYm9vbGVhbikgPT4gdm9pZDtcclxufTtcclxuXHJcbi8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcclxuZXhwb3J0IGNvbnN0IGVycm9yU3JjV2l0aG91dEh0dHBDbGllbnQgPSAnW25neC1tYXJrZG93bl0gV2hlbiB1c2luZyB0aGUgW3NyY10gYXR0cmlidXRlIHlvdSAqaGF2ZSB0byogcGFzcyB0aGUgYEh0dHBDbGllbnRgIGFzIGEgcGFyYW1ldGVyIG9mIHRoZSBgZm9yUm9vdGAgbWV0aG9kLiBTZWUgUkVBRE1FIGZvciBtb3JlIGluZm9ybWF0aW9uJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1hcmtkb3duU2VydmljZSB7XHJcbiAgZ2V0IHJlbmRlcmVyKCk6IFJlbmRlcmVyIHtcclxuICAgIHJldHVybiB0aGlzLm9wdGlvbnMucmVuZGVyZXI7XHJcbiAgfVxyXG4gIHNldCByZW5kZXJlcih2YWx1ZTogbWFya2VkLlJlbmRlcmVyKSB7XHJcbiAgICB0aGlzLm9wdGlvbnMucmVuZGVyZXIgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgQE9wdGlvbmFsKCkgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgcHVibGljIG9wdGlvbnM6IE1hcmtlZE9wdGlvbnMsXHJcbiAgKSB7XHJcbiAgICBpZiAoIXRoaXMucmVuZGVyZXIpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlciA9IG5ldyBSZW5kZXJlcigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29tcGlsZShtYXJrZG93bjogc3RyaW5nLCBtYXJrZWRPcHRpb25zID0gdGhpcy5vcHRpb25zKSB7XHJcbiAgICBjb25zdCBwcmVjb21waWxlZCA9IHRoaXMucHJlY29tcGlsZShtYXJrZG93bik7XHJcbiAgICByZXR1cm4gcGFyc2UocHJlY29tcGlsZWQsIG1hcmtlZE9wdGlvbnMpO1xyXG4gIH1cclxuXHJcbiAgZ2V0U291cmNlKHNyYzogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMuaHR0cCkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3JTcmNXaXRob3V0SHR0cENsaWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cFxyXG4gICAgICAuZ2V0KHNyYywgeyByZXNwb25zZVR5cGU6ICd0ZXh0JyB9KVxyXG4gICAgICAucGlwZShtYXAobWFya2Rvd24gPT4gdGhpcy5oYW5kbGVFeHRlbnNpb24oc3JjLCBtYXJrZG93bikpKTtcclxuICB9XHJcblxyXG4gIGhpZ2hsaWdodCgpIHtcclxuICAgIGlmICh0eXBlb2YgUHJpc20gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgIFByaXNtLmhpZ2hsaWdodEFsbChmYWxzZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUV4dGVuc2lvbihzcmM6IHN0cmluZywgbWFya2Rvd246IHN0cmluZykge1xyXG4gICAgY29uc3QgZXh0ZW5zaW9uID0gc3JjXHJcbiAgICAgID8gc3JjLnNwbGl0KCcuJykuc3BsaWNlKC0xKS5qb2luKClcclxuICAgICAgOiBudWxsO1xyXG4gICAgcmV0dXJuIGV4dGVuc2lvbiAhPT0gJ21kJ1xyXG4gICAgICA/ICdgYGAnICsgZXh0ZW5zaW9uICsgJ1xcbicgKyBtYXJrZG93biArICdcXG5gYGAnXHJcbiAgICAgIDogbWFya2Rvd247XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZWNvbXBpbGUobWFya2Rvd246IHN0cmluZykge1xyXG4gICAgaWYgKCFtYXJrZG93bikge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbiAgICBsZXQgaW5kZW50U3RhcnQ6IG51bWJlcjtcclxuICAgIHJldHVybiBtYXJrZG93blxyXG4gICAgICAucmVwbGFjZSgvXFwmZ3Q7L2csICc+JylcclxuICAgICAgLnNwbGl0KCdcXG4nKVxyXG4gICAgICAubWFwKGxpbmUgPT4ge1xyXG4gICAgICAgIC8vIGZpbmQgcG9zaXRpb24gb2YgMXN0IG5vbi13aGl0ZXNwYWNlIGNoYXJhY3RlclxyXG4gICAgICAgIC8vIHRvIGRldGVybWluZSB0aGUgbWFya2Rvd24gaW5kZW50YXRpb24gc3RhcnRcclxuICAgICAgICBpZiAobGluZS5sZW5ndGggPiAwICYmIGlzTmFOKGluZGVudFN0YXJ0KSkge1xyXG4gICAgICAgICAgaW5kZW50U3RhcnQgPSBsaW5lLnNlYXJjaCgvXFxTfCQvKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gcmVtb3ZlIHdoaXRlc3BhY2VzIGJlZm9yZSBpbmRlbnRhdGlvbiBzdGFydFxyXG4gICAgICAgIHJldHVybiBpbmRlbnRTdGFydFxyXG4gICAgICAgICAgPyBsaW5lLnN1YnN0cmluZyhpbmRlbnRTdGFydClcclxuICAgICAgICAgIDogbGluZTtcclxuICAgICAgfSkuam9pbignXFxuJyk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEFmdGVyVmlld0luaXQsIENvbXBvbmVudCwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlbmRlcmVyIH0gZnJvbSAnbWFya2VkJztcclxuaW1wb3J0IHsgTWFya2Rvd25TZXJ2aWNlIH0gZnJvbSAnLi9tYXJrZG93bi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpjb21wb25lbnQtc2VsZWN0b3JcclxuICBzZWxlY3RvcjogJ21hcmtkb3duLCBbbWFya2Rvd25dJyxcclxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxyXG4gIHN0eWxlczogW2A6aG9zdCAvZGVlcC8gdGFibGV7Ym9yZGVyLXNwYWNpbmc6MDtib3JkZXItY29sbGFwc2U6Y29sbGFwc2U7bWFyZ2luLWJvdHRvbToxNnB4fTpob3N0IC9kZWVwLyB0YWJsZSB0ZCw6aG9zdCAvZGVlcC8gdGFibGUgdGh7cGFkZGluZzo2cHggMTNweDtib3JkZXI6MXB4IHNvbGlkICNkZGR9Omhvc3QgL2RlZXAvIHRhYmxlIHRkW2FsaWduPWxlZnRdLDpob3N0IC9kZWVwLyB0YWJsZSB0aFthbGlnbj1sZWZ0XXt0ZXh0LWFsaWduOmxlZnR9Omhvc3QgL2RlZXAvIHRhYmxlIHRkW2FsaWduPWNlbnRlcl0sOmhvc3QgL2RlZXAvIHRhYmxlIHRoW2FsaWduPWNlbnRlcl17dGV4dC1hbGlnbjpjZW50ZXJ9Omhvc3QgL2RlZXAvIHRhYmxlIHRkW2FsaWduPXJpZ2h0XSw6aG9zdCAvZGVlcC8gdGFibGUgdGhbYWxpZ249cmlnaHRde3RleHQtYWxpZ246cmlnaHR9Omhvc3QgL2RlZXAvIHRhYmxlIHRyOm50aC1jaGlsZCgybil7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDAsMCwwLC4wMyl9Omhvc3QgL2RlZXAvIGJsb2NrcXVvdGV7cGFkZGluZzowIDFlbTtjb2xvcjpyZ2JhKDAsMCwwLC41MzUpO2JvcmRlci1sZWZ0Oi4yNWVtIHNvbGlkIHJnYmEoMCwwLDAsLjExKX1gXSxcclxuICBwcmVzZXJ2ZVdoaXRlc3BhY2VzOiB0cnVlLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFya2Rvd25Db21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcclxuICBwcml2YXRlIF9kYXRhOiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBfc3JjOiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IGRhdGEoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9kYXRhO1xyXG4gIH1cclxuXHJcbiAgc2V0IGRhdGEodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5fZGF0YSA9IHZhbHVlO1xyXG4gICAgdGhpcy5yZW5kZXIodmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBnZXQgc3JjKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fc3JjO1xyXG4gIH1cclxuXHJcbiAgc2V0IHNyYyh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLl9zcmMgPSB2YWx1ZTtcclxuICAgIHRoaXMubWFya2Rvd25TZXJ2aWNlXHJcbiAgICAgIC5nZXRTb3VyY2UodmFsdWUpXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgbWFya2Rvd24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5yZW5kZXIobWFya2Rvd24pO1xyXG4gICAgICAgICAgdGhpcy5sb2FkLmVtaXQobWFya2Rvd24pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZXJyb3IgPT4gdGhpcy5lcnJvci5lbWl0KGVycm9yKSxcclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIEBJbnB1dCgpIGlzVGFyZ2V0QmxhbmtMaW5rcyA9IGZhbHNlO1xyXG5cclxuICBAT3V0cHV0KCkgZXJyb3IgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuICBAT3V0cHV0KCkgbG9hZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG5cclxuICBnZXQgaXNUcmFuc2NsdWRlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhdGhpcy5kYXRhICYmICF0aGlzLnNyYztcclxuICB9XHJcblxyXG4gIHN0YXRpYyBhZGRUYXJnZXRCbGFuayhocmVmOiBzdHJpbmcsIHRpdGxlOiBzdHJpbmcsIHRleHQ6IHN0cmluZykge1xyXG4gICAgbGV0IG91dDtcclxuICAgIG91dCA9ICc8YSBocmVmPVwiJyArIGhyZWYgKyAnXCInO1xyXG4gICAgb3V0ICs9ICcgdGFyZ2V0PVwiX2JsYW5rXCInO1xyXG4gICAgaWYgKHRpdGxlKSB7XHJcbiAgICAgIG91dCArPSAnIHRpdGxlPVwiJyArIHRpdGxlICsgJ1wiJztcclxuICAgIH1cclxuICAgIHJldHVybiBvdXQgKyAnPicgKyB0ZXh0ICsgJzwvYT4nO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZWxlbWVudDogRWxlbWVudFJlZixcclxuICAgIHB1YmxpYyBtYXJrZG93blNlcnZpY2U6IE1hcmtkb3duU2VydmljZSxcclxuICApIHtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIGlmICh0aGlzLmlzVGFyZ2V0QmxhbmtMaW5rcykge1xyXG4gICAgICBjb25zdCBjdXN0b21SZW5kZXJlciA9IG5ldyBSZW5kZXJlcigpO1xyXG4gICAgICBjdXN0b21SZW5kZXJlci5saW5rID0gTWFya2Rvd25Db21wb25lbnQuYWRkVGFyZ2V0Qmxhbms7XHJcbiAgICAgIHRoaXMubWFya2Rvd25TZXJ2aWNlLnJlbmRlcmVyID0gY3VzdG9tUmVuZGVyZXI7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5pc1RyYW5zY2x1ZGVkKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyKHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmlubmVySFRNTCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW5kZXIobWFya2Rvd246IHN0cmluZykge1xyXG4gICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MID0gdGhpcy5tYXJrZG93blNlcnZpY2UuY29tcGlsZShtYXJrZG93bik7XHJcbiAgICB0aGlzLm1hcmtkb3duU2VydmljZS5oaWdobGlnaHQoKTtcclxuICB9XHJcbn1cclxuIiwiLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuLyogZ2xvYmFsIFJlZmxlY3QsIFByb21pc2UgKi9cclxuXHJcbnZhciBleHRlbmRTdGF0aWNzID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8XHJcbiAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICBmdW5jdGlvbiAoZCwgYikgeyBmb3IgKHZhciBwIGluIGIpIGlmIChiLmhhc093blByb3BlcnR5KHApKSBkW3BdID0gYltwXTsgfTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4dGVuZHMoZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgZC5wcm90b3R5cGUgPSBiID09PSBudWxsID8gT2JqZWN0LmNyZWF0ZShiKSA6IChfXy5wcm90b3R5cGUgPSBiLnByb3RvdHlwZSwgbmV3IF9fKCkpO1xyXG59XHJcblxyXG5leHBvcnQgdmFyIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiBfX2Fzc2lnbih0KSB7XHJcbiAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xyXG4gICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSkgdFtwXSA9IHNbcF07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVzdChzLCBlKSB7XHJcbiAgICB2YXIgdCA9IHt9O1xyXG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXHJcbiAgICAgICAgdFtwXSA9IHNbcF07XHJcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDApXHJcbiAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xyXG4gICAgcmV0dXJuIHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2RlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19wYXJhbShwYXJhbUluZGV4LCBkZWNvcmF0b3IpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBrZXkpIHsgZGVjb3JhdG9yKHRhcmdldCwga2V5LCBwYXJhbUluZGV4KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSkge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXRlcih0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcclxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUocmVzdWx0LnZhbHVlKTsgfSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxyXG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSB5W29wWzBdICYgMiA/IFwicmV0dXJuXCIgOiBvcFswXSA/IFwidGhyb3dcIiA6IFwibmV4dFwiXSkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbMCwgdC52YWx1ZV07XHJcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxyXG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19leHBvcnRTdGFyKG0sIGV4cG9ydHMpIHtcclxuICAgIGZvciAodmFyIHAgaW4gbSkgaWYgKCFleHBvcnRzLmhhc093blByb3BlcnR5KHApKSBleHBvcnRzW3BdID0gbVtwXTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fdmFsdWVzKG8pIHtcclxuICAgIHZhciBtID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9bU3ltYm9sLml0ZXJhdG9yXSwgaSA9IDA7XHJcbiAgICBpZiAobSkgcmV0dXJuIG0uY2FsbChvKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgbmV4dDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAobyAmJiBpID49IG8ubGVuZ3RoKSBvID0gdm9pZCAwO1xyXG4gICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogbyAmJiBvW2krK10sIGRvbmU6ICFvIH07XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVhZChvLCBuKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl07XHJcbiAgICBpZiAoIW0pIHJldHVybiBvO1xyXG4gICAgdmFyIGkgPSBtLmNhbGwobyksIHIsIGFyID0gW10sIGU7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIHdoaWxlICgobiA9PT0gdm9pZCAwIHx8IG4tLSA+IDApICYmICEociA9IGkubmV4dCgpKS5kb25lKSBhci5wdXNoKHIudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgY2F0Y2ggKGVycm9yKSB7IGUgPSB7IGVycm9yOiBlcnJvciB9OyB9XHJcbiAgICBmaW5hbGx5IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAociAmJiAhci5kb25lICYmIChtID0gaVtcInJldHVyblwiXSkpIG0uY2FsbChpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmluYWxseSB7IGlmIChlKSB0aHJvdyBlLmVycm9yOyB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3NwcmVhZCgpIHtcclxuICAgIGZvciAodmFyIGFyID0gW10sIGkgPSAwOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgIGFyID0gYXIuY29uY2F0KF9fcmVhZChhcmd1bWVudHNbaV0pKTtcclxuICAgIHJldHVybiBhcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXQodikge1xyXG4gICAgcmV0dXJuIHRoaXMgaW5zdGFuY2VvZiBfX2F3YWl0ID8gKHRoaXMudiA9IHYsIHRoaXMpIDogbmV3IF9fYXdhaXQodik7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jR2VuZXJhdG9yKHRoaXNBcmcsIF9hcmd1bWVudHMsIGdlbmVyYXRvcikge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBnID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pLCBpLCBxID0gW107XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaWYgKGdbbl0pIGlbbl0gPSBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKGEsIGIpIHsgcS5wdXNoKFtuLCB2LCBhLCBiXSkgPiAxIHx8IHJlc3VtZShuLCB2KTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHJlc3VtZShuLCB2KSB7IHRyeSB7IHN0ZXAoZ1tuXSh2KSk7IH0gY2F0Y2ggKGUpIHsgc2V0dGxlKHFbMF1bM10sIGUpOyB9IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAocikgeyByLnZhbHVlIGluc3RhbmNlb2YgX19hd2FpdCA/IFByb21pc2UucmVzb2x2ZShyLnZhbHVlLnYpLnRoZW4oZnVsZmlsbCwgcmVqZWN0KSA6IHNldHRsZShxWzBdWzJdLCByKTsgIH1cclxuICAgIGZ1bmN0aW9uIGZ1bGZpbGwodmFsdWUpIHsgcmVzdW1lKFwibmV4dFwiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHJlamVjdCh2YWx1ZSkgeyByZXN1bWUoXCJ0aHJvd1wiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShmLCB2KSB7IGlmIChmKHYpLCBxLnNoaWZ0KCksIHEubGVuZ3RoKSByZXN1bWUocVswXVswXSwgcVswXVsxXSk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNEZWxlZ2F0b3Iobykge1xyXG4gICAgdmFyIGksIHA7XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIsIGZ1bmN0aW9uIChlKSB7IHRocm93IGU7IH0pLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuLCBmKSB7IGlmIChvW25dKSBpW25dID0gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIChwID0gIXApID8geyB2YWx1ZTogX19hd2FpdChvW25dKHYpKSwgZG9uZTogbiA9PT0gXCJyZXR1cm5cIiB9IDogZiA/IGYodikgOiB2OyB9OyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jVmFsdWVzKG8pIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgbSA9IG9bU3ltYm9sLmFzeW5jSXRlcmF0b3JdO1xyXG4gICAgcmV0dXJuIG0gPyBtLmNhbGwobykgOiB0eXBlb2YgX192YWx1ZXMgPT09IFwiZnVuY3Rpb25cIiA/IF9fdmFsdWVzKG8pIDogb1tTeW1ib2wuaXRlcmF0b3JdKCk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ha2VUZW1wbGF0ZU9iamVjdChjb29rZWQsIHJhdykge1xyXG4gICAgaWYgKE9iamVjdC5kZWZpbmVQcm9wZXJ0eSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkoY29va2VkLCBcInJhd1wiLCB7IHZhbHVlOiByYXcgfSk7IH0gZWxzZSB7IGNvb2tlZC5yYXcgPSByYXc7IH1cclxuICAgIHJldHVybiBjb29rZWQ7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnRTdGFyKG1vZCkge1xyXG4gICAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcclxuICAgIHZhciByZXN1bHQgPSB7fTtcclxuICAgIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIGlmIChPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSByZXN1bHRba10gPSBtb2Rba107XHJcbiAgICByZXN1bHQuZGVmYXVsdCA9IG1vZDtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydERlZmF1bHQobW9kKSB7XHJcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IGRlZmF1bHQ6IG1vZCB9O1xyXG59XHJcbiIsImltcG9ydCB7IE5nWm9uZSwgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTWFya2Rvd25TZXJ2aWNlIH0gZnJvbSAnLi9tYXJrZG93bi5zZXJ2aWNlJztcclxuXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnbWFya2Rvd24nLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFya2Rvd25QaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBtYXJrZG93blNlcnZpY2U6IE1hcmtkb3duU2VydmljZSxcclxuICAgIHByaXZhdGUgem9uZTogTmdab25lLFxyXG4gICkgeyB9XHJcblxyXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoYE1hcmtkb3duUGlwZSBoYXMgYmVlbiBpbnZva2VkIHdpdGggYW4gaW52YWxpZCB2YWx1ZSB0eXBlIFske3ZhbHVlfV1gKTtcclxuICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IG1hcmtkb3duID0gdGhpcy5tYXJrZG93blNlcnZpY2UuY29tcGlsZSh2YWx1ZSk7XHJcblxyXG4gICAgdGhpcy56b25lLnJ1bk91dHNpZGVBbmd1bGFyKCgpID0+IHtcclxuICAgICAgLy8gZ2xpdGNoIGluIHRoZSBVSS4uLiBuZWVkIGEgYmV0dGVyIHdheSB0byBoYW5kbGUgdGhpcyFcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLm1hcmtkb3duU2VydmljZS5oaWdobGlnaHQoKSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gbWFya2Rvd247XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBQcm92aWRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTGFuZ3VhZ2VQaXBlIH0gZnJvbSAnLi9sYW5ndWFnZS5waXBlJztcclxuaW1wb3J0IHsgTWFya2Rvd25Db21wb25lbnQgfSBmcm9tICcuL21hcmtkb3duLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hcmtkb3duUGlwZSB9IGZyb20gJy4vbWFya2Rvd24ucGlwZSc7XHJcbmltcG9ydCB7IE1hcmtkb3duU2VydmljZSB9IGZyb20gJy4vbWFya2Rvd24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1hcmtlZE9wdGlvbnMgfSBmcm9tICcuL21hcmtlZC1vcHRpb25zJztcclxuXHJcbi8vIGhhdmluZyBhIGRlcGVuZGVuY3kgb24gYEh0dHBDbGllbnRNb2R1bGVgIHdpdGhpbiBhIGxpYnJhcnlcclxuLy8gYnJlYWtzIGFsbCB0aGUgaW50ZXJjZXB0b3JzIGZyb20gdGhlIGFwcCBjb25zdW1pbmcgdGhlIGxpYnJhcnlcclxuLy8gaGVyZSwgd2UgZXhwbGljaXRlbHkgYXNrIHRoZSB1c2VyIHRvIHBhc3MgYSBwcm92aWRlciB3aXRoXHJcbi8vIHRoZWlyIG93biBpbnN0YW5jZSBvZiBgSHR0cENsaWVudE1vZHVsZWBcclxuZXhwb3J0IGludGVyZmFjZSBNYXJrZG93bk1vZHVsZUNvbmZpZyB7XHJcbiAgbG9hZGVyPzogUHJvdmlkZXI7XHJcbiAgbWFya2VkT3B0aW9ucz86IFByb3ZpZGVyO1xyXG59XHJcblxyXG5leHBvcnQgY29uc3QgaW5pdGlhbE1hcmtlZE9wdGlvbnM6IFByb3ZpZGVyID0ge1xyXG4gIHByb3ZpZGU6IE1hcmtlZE9wdGlvbnMsXHJcbiAgdXNlVmFsdWU6IHtcclxuICAgIGdmbTogdHJ1ZSxcclxuICAgIHRhYmxlczogdHJ1ZSxcclxuICAgIGJyZWFrczogZmFsc2UsXHJcbiAgICBwZWRhbnRpYzogZmFsc2UsXHJcbiAgICBzYW5pdGl6ZTogZmFsc2UsXHJcbiAgICBzbWFydExpc3RzOiB0cnVlLFxyXG4gICAgc21hcnR5cGFudHM6IGZhbHNlLFxyXG4gIH0sXHJcbn07XHJcblxyXG5jb25zdCBzaGFyZWREZWNsYXJhdGlvbnMgPSBbXHJcbiAgTGFuZ3VhZ2VQaXBlLFxyXG4gIE1hcmtkb3duQ29tcG9uZW50LFxyXG4gIE1hcmtkb3duUGlwZSxcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZXhwb3J0czogW1xyXG4gICAgLi4uc2hhcmVkRGVjbGFyYXRpb25zLFxyXG4gIF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAuLi5zaGFyZWREZWNsYXJhdGlvbnMsXHJcbiAgXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIE1hcmtkb3duTW9kdWxlIHtcclxuICBzdGF0aWMgZm9yUm9vdChtYXJrZG93bk1vZHVsZUNvbmZpZz86IE1hcmtkb3duTW9kdWxlQ29uZmlnKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBuZ01vZHVsZTogTWFya2Rvd25Nb2R1bGUsXHJcbiAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIE1hcmtkb3duU2VydmljZSxcclxuICAgICAgICAuLi4obWFya2Rvd25Nb2R1bGVDb25maWdcclxuICAgICAgICAgID8gW1xyXG4gICAgICAgICAgICAgIG1hcmtkb3duTW9kdWxlQ29uZmlnLmxvYWRlciB8fCBbXSxcclxuICAgICAgICAgICAgICBtYXJrZG93bk1vZHVsZUNvbmZpZy5tYXJrZWRPcHRpb25zIHx8IGluaXRpYWxNYXJrZWRPcHRpb25zLFxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgICA6IFtpbml0aWFsTWFya2VkT3B0aW9uc10pLFxyXG4gICAgICBdLFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHN0YXRpYyBmb3JDaGlsZCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBNYXJrZG93bk1vZHVsZSxcclxuICAgIH07XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IFJlbmRlcmVyIH0gZnJvbSAnbWFya2VkJztcclxuXHJcbmV4cG9ydCBjbGFzcyBNYXJrZWRSZW5kZXJlciBleHRlbmRzIFJlbmRlcmVyIHsgfVxyXG4iXSwibmFtZXMiOlsiUGlwZSIsImh0dHAiLCJSZW5kZXJlciIsInBhcnNlIiwibWFwIiwiSW5qZWN0YWJsZSIsIkh0dHBDbGllbnQiLCJPcHRpb25hbCIsIkV2ZW50RW1pdHRlciIsIkNvbXBvbmVudCIsIkVsZW1lbnRSZWYiLCJJbnB1dCIsIk91dHB1dCIsIk5nWm9uZSIsIk5nTW9kdWxlIiwidHNsaWJfMS5fX2V4dGVuZHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7UUFPRSxnQ0FBUzs7Ozs7WUFBVCxVQUFVLEtBQWEsRUFBRSxRQUFnQjtnQkFDdkMsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBQzdCLE9BQU8sQ0FBQyxLQUFLLENBQUMsK0RBQTZELEtBQUssTUFBRyxDQUFDLENBQUM7b0JBQ3JGLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2dCQUNELElBQUksT0FBTyxRQUFRLEtBQUssUUFBUSxFQUFFO29CQUNoQyxPQUFPLENBQUMsS0FBSyxDQUFDLDhEQUE0RCxRQUFRLE1BQUcsQ0FBQyxDQUFDO29CQUN2RixPQUFPLEtBQUssQ0FBQztpQkFDZDtnQkFDRCxPQUFPLEtBQUssR0FBRyxRQUFRLEdBQUcsSUFBSSxHQUFJLEtBQUssR0FBRyxPQUFPLENBQUM7YUFDbkQ7O29CQWZGQSxTQUFJLFNBQUM7d0JBQ0osSUFBSSxFQUFFLFVBQVU7cUJBQ2pCOzsyQkFKRDs7Ozs7OztBQ0VBLFFBQUE7Ozs0QkFGQTtRQStFQzs7Ozs7O0FDL0VEO0FBY0EseUJBQWEseUJBQXlCLEdBQUcsMkpBQTJKLENBQUM7O1FBV25NLHlCQUNzQkMsT0FBZ0IsRUFDN0I7WUFEYSxTQUFJLEdBQUpBLE9BQUksQ0FBWTtZQUM3QixZQUFPLEdBQVAsT0FBTztZQUVkLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUlDLGVBQVEsRUFBRSxDQUFDO2FBQ2hDO1NBQ0Y7UUFkRCxzQkFBSSxxQ0FBUTs7O2dCQUFaO2dCQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7YUFDOUI7Ozs7Z0JBQ0QsVUFBYSxLQUFzQjtnQkFDakMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2FBQy9COzs7V0FIQTs7Ozs7O1FBY0QsaUNBQU87Ozs7O1lBQVAsVUFBUSxRQUFnQixFQUFFLGFBQTRCO2dCQUE1Qiw4QkFBQTtvQkFBQSxnQkFBZ0IsSUFBSSxDQUFDLE9BQU87O2dCQUNwRCxxQkFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUMsT0FBT0MsWUFBSyxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQzthQUMxQzs7Ozs7UUFFRCxtQ0FBUzs7OztZQUFULFVBQVUsR0FBVztnQkFBckIsaUJBUUM7Z0JBUEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ2QsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO2lCQUM1QztnQkFFRCxPQUFPLElBQUksQ0FBQyxJQUFJO3FCQUNiLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLENBQUM7cUJBQ2xDLElBQUksQ0FBQ0MsYUFBRyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLEdBQUEsQ0FBQyxDQUFDLENBQUM7YUFDL0Q7Ozs7UUFFRCxtQ0FBUzs7O1lBQVQ7Z0JBQ0UsSUFBSSxPQUFPLEtBQUssS0FBSyxXQUFXLEVBQUU7b0JBQ2hDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzNCO2FBQ0Y7Ozs7OztRQUVPLHlDQUFlOzs7OztzQkFBQyxHQUFXLEVBQUUsUUFBZ0I7Z0JBQ25ELHFCQUFNLFNBQVMsR0FBRyxHQUFHO3NCQUNqQixHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtzQkFDaEMsSUFBSSxDQUFDO2dCQUNULE9BQU8sU0FBUyxLQUFLLElBQUk7c0JBQ3JCLEtBQUssR0FBRyxTQUFTLEdBQUcsSUFBSSxHQUFHLFFBQVEsR0FBRyxPQUFPO3NCQUM3QyxRQUFRLENBQUM7Ozs7OztRQUdQLG9DQUFVOzs7O3NCQUFDLFFBQWdCO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNiLE9BQU8sRUFBRSxDQUFDO2lCQUNYO2dCQUNELHFCQUFJLFdBQW1CLENBQUM7Z0JBQ3hCLE9BQU8sUUFBUTtxQkFDWixPQUFPLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQztxQkFDdEIsS0FBSyxDQUFDLElBQUksQ0FBQztxQkFDWCxHQUFHLENBQUMsVUFBQSxJQUFJOzs7b0JBR1AsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLEVBQUU7d0JBQ3pDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUNuQzs7b0JBRUQsT0FBTyxXQUFXOzBCQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDOzBCQUMzQixJQUFJLENBQUM7aUJBQ1YsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs7O29CQWxFbkJDLGVBQVU7Ozs7O3dCQWhCRkMsZUFBVSx1QkEwQmRDLGFBQVE7d0JBcEJKLGFBQWE7Ozs4QkFOdEI7Ozs7Ozs7QUNBQTtRQThERSwyQkFDUyxTQUNBO1lBREEsWUFBTyxHQUFQLE9BQU87WUFDUCxvQkFBZSxHQUFmLGVBQWU7c0NBckJNLEtBQUs7eUJBRWpCLElBQUlDLGlCQUFZLEVBQVU7d0JBQzNCLElBQUlBLGlCQUFZLEVBQVU7U0FvQjFDO1FBbkRELHNCQUNJLG1DQUFJOzs7Z0JBRFI7Z0JBRUUsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ25COzs7O2dCQUVELFVBQVMsS0FBYTtnQkFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDcEI7OztXQUxBO1FBT0Qsc0JBQ0ksa0NBQUc7OztnQkFEUDtnQkFFRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDbEI7Ozs7Z0JBRUQsVUFBUSxLQUFhO2dCQUFyQixpQkFXQztnQkFWQyxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztnQkFDbEIsSUFBSSxDQUFDLGVBQWU7cUJBQ2pCLFNBQVMsQ0FBQyxLQUFLLENBQUM7cUJBQ2hCLFNBQVMsQ0FDUixVQUFBLFFBQVE7b0JBQ04sS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDdEIsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQzFCLEVBQ0QsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBQSxDQUNoQyxDQUFDO2FBQ0w7OztXQWJBO1FBb0JELHNCQUFJLDRDQUFhOzs7Z0JBQWpCO2dCQUNFLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQzthQUNoQzs7O1dBQUE7Ozs7Ozs7UUFFTSxnQ0FBYzs7Ozs7O1lBQXJCLFVBQXNCLElBQVksRUFBRSxLQUFhLEVBQUUsSUFBWTtnQkFDN0QscUJBQUksR0FBRyxDQUFDO2dCQUNSLEdBQUcsR0FBRyxXQUFXLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQztnQkFDL0IsR0FBRyxJQUFJLGtCQUFrQixDQUFDO2dCQUMxQixJQUFJLEtBQUssRUFBRTtvQkFDVCxHQUFHLElBQUksVUFBVSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7aUJBQ2pDO2dCQUNELE9BQU8sR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDO2FBQ2xDOzs7O1FBUUQsMkNBQWU7OztZQUFmO2dCQUNFLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO29CQUMzQixxQkFBTSxjQUFjLEdBQUcsSUFBSU4sZUFBUSxFQUFFLENBQUM7b0JBQ3RDLGNBQWMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDO29CQUN2RCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUM7aUJBQ2hEO2dCQUNELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDbkQ7YUFDRjs7Ozs7UUFFRCxrQ0FBTTs7OztZQUFOLFVBQU8sUUFBZ0I7Z0JBQ3JCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNsQzs7b0JBOUVGTyxjQUFTLFNBQUM7O3dCQUVULFFBQVEsRUFBRSxzQkFBc0I7d0JBQ2hDLFFBQVEsRUFBRSwyQkFBMkI7d0JBQ3JDLE1BQU0sRUFBRSxDQUFDLG9sQkFBb2xCLENBQUM7d0JBQzlsQixtQkFBbUIsRUFBRSxJQUFJO3FCQUMxQjs7Ozs7d0JBVmtDQyxlQUFVO3dCQUVwQyxlQUFlOzs7OzJCQWFyQkMsVUFBSzswQkFVTEEsVUFBSzt5Q0FrQkxBLFVBQUs7NEJBRUxDLFdBQU07MkJBQ05BLFdBQU07O2dDQTlDVDs7O0lDQUE7Ozs7Ozs7Ozs7Ozs7O0lBY0E7SUFFQSxJQUFJLGFBQWEsR0FBRyxNQUFNLENBQUMsY0FBYztTQUNwQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsWUFBWSxLQUFLLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM1RSxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQUUsSUFBSSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztBQUUvRSx1QkFBMEIsQ0FBQyxFQUFFLENBQUM7UUFDMUIsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQixnQkFBZ0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsRUFBRTtRQUN2QyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsS0FBSyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7QUFFRCxvQkFxRnVCLENBQUMsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxHQUFHLE9BQU8sTUFBTSxLQUFLLFVBQVUsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDakMsSUFBSTtZQUNBLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLElBQUk7Z0JBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUU7UUFDRCxPQUFPLEtBQUssRUFBRTtZQUFFLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQztTQUFFO2dCQUMvQjtZQUNKLElBQUk7Z0JBQ0EsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwRDtvQkFDTztnQkFBRSxJQUFJLENBQUM7b0JBQUUsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQUU7U0FDcEM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7QUFFRDtRQUNJLEtBQUssSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO1lBQzlDLEVBQUUsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3pDLE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7O0FDcElEO1FBU0Usc0JBQ1UsaUJBQ0E7WUFEQSxvQkFBZSxHQUFmLGVBQWU7WUFDZixTQUFJLEdBQUosSUFBSTtTQUNUOzs7OztRQUVMLGdDQUFTOzs7O1lBQVQsVUFBVSxLQUFhO2dCQUF2QixpQkFjQztnQkFiQyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtvQkFDN0IsT0FBTyxDQUFDLEtBQUssQ0FBQywrREFBNkQsS0FBSyxNQUFHLENBQUMsQ0FBQztvQkFDckYsT0FBTyxLQUFLLENBQUM7aUJBQ2Q7Z0JBRUQscUJBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUVyRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDOztvQkFFMUIsVUFBVSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxHQUFBLENBQUMsQ0FBQztpQkFDcEQsQ0FBQyxDQUFDO2dCQUVILE9BQU8sUUFBUSxDQUFDO2FBQ2pCOztvQkF4QkZaLFNBQUksU0FBQzt3QkFDSixJQUFJLEVBQUUsVUFBVTtxQkFDakI7Ozs7O3dCQUpRLGVBQWU7d0JBRmZhLFdBQU07OzsyQkFBZjs7Ozs7Ozt5QkNrQmEsb0JBQW9CLEdBQWE7UUFDNUMsT0FBTyxFQUFFLGFBQWE7UUFDdEIsUUFBUSxFQUFFO1lBQ1IsR0FBRyxFQUFFLElBQUk7WUFDVCxNQUFNLEVBQUUsSUFBSTtZQUNaLE1BQU0sRUFBRSxLQUFLO1lBQ2IsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFdBQVcsRUFBRSxLQUFLO1NBQ25CO0tBQ0YsQ0FBQztJQUVGLHFCQUFNLGtCQUFrQixHQUFHO1FBQ3pCLFlBQVk7UUFDWixpQkFBaUI7UUFDakIsWUFBWTtLQUNiLENBQUM7Ozs7Ozs7O1FBV08sc0JBQU87Ozs7WUFBZCxVQUFlLG9CQUEyQztnQkFDeEQsT0FBTztvQkFDTCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsU0FBUzt3QkFDUCxlQUFlO3dCQUNYLG9CQUFvQjswQkFDcEI7NEJBQ0Usb0JBQW9CLENBQUMsTUFBTSxJQUFJLEVBQUU7NEJBQ2pDLG9CQUFvQixDQUFDLGFBQWEsSUFBSSxvQkFBb0I7eUJBQzNEOzBCQUNELENBQUMsb0JBQW9CLENBQUMsRUFDM0I7aUJBQ0YsQ0FBQzthQUNIOzs7O1FBRU0sdUJBQVE7OztZQUFmO2dCQUNFLE9BQU87b0JBQ0wsUUFBUSxFQUFFLGNBQWM7aUJBQ3pCLENBQUM7YUFDSDs7b0JBNUJGQyxhQUFRLFNBQUM7d0JBQ1IsT0FBTyxXQUNGLGtCQUFrQixDQUN0Qjt3QkFDRCxZQUFZLFdBQ1Asa0JBQWtCLENBQ3RCO3FCQUNGOzs2QkE1Q0Q7Ozs7Ozs7UUNFQTtRQUFvQ0Msa0NBQVE7Ozs7NkJBRjVDO01BRW9DYixlQUFRLEVBQUk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9