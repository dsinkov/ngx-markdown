import { AfterViewInit, ElementRef, EventEmitter } from '@angular/core';
import { MarkdownService } from './markdown.service';
export declare class MarkdownComponent implements AfterViewInit {
    element: ElementRef;
    markdownService: MarkdownService;
    private _data;
    private _src;
    data: string;
    src: string;
    isTargetBlankLinks: boolean;
    error: EventEmitter<string>;
    load: EventEmitter<string>;
    readonly isTranscluded: boolean;
    static addTargetBlank(href: string, title: string, text: string): string;
    constructor(element: ElementRef, markdownService: MarkdownService);
    ngAfterViewInit(): void;
    render(markdown: string): void;
}
