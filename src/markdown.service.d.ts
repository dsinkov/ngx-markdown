/// <reference types="marked" />
import { HttpClient } from '@angular/common/http';
import { Renderer } from 'marked';
import { Observable } from 'rxjs';
import { MarkedOptions } from './marked-options';
export declare const errorSrcWithoutHttpClient = "[ngx-markdown] When using the [src] attribute you *have to* pass the `HttpClient` as a parameter of the `forRoot` method. See README for more information";
export declare class MarkdownService {
    private http;
    options: MarkedOptions;
    renderer: Renderer;
    constructor(http: HttpClient, options: MarkedOptions);
    compile(markdown: string, markedOptions?: MarkedOptions): string;
    getSource(src: string): Observable<string>;
    highlight(): void;
    private handleExtension(src, markdown);
    private precompile(markdown);
}
