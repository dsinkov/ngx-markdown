import { NgZone, PipeTransform } from '@angular/core';
import { MarkdownService } from './markdown.service';
export declare class MarkdownPipe implements PipeTransform {
    private markdownService;
    private zone;
    constructor(markdownService: MarkdownService, zone: NgZone);
    transform(value: string): string;
}
